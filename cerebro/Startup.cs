﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using cerebro.Models;
using cerebro.Services;


using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace cerebro
{
    public class Startup
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Startup));
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public async void ConfigureServices(IServiceCollection services)
        {
            
            services.AddDbContext<dbCerebroContext>(options => {
                //options.UseSqlServer("Server=localhost;Database=dbCerebro;Uid=usrCerebro;Pwd=usrCerebro21..;");
                options.UseSqlServer(Configuration.GetConnectionString("CerebroDatabase"));
                //options.UseSqlServer(Configuration["ConnectionStrings:CerebroDatabase"]);
            });
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

            //Application Services
            services.AddScoped<IParametroService, ParametroServiceImpl>();

            services.AddDistributedMemoryCache();
            services.AddMemoryCache();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(20);
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddRazorOptions(options => options.AllowRecompilingViewsOnFileChange = true);
            /*
            services.AddSingleton<IJobFactory, JobFactoryCerebro>();
            services.AddSingleton<RevisaEstadoTask>();*/
            services.AddSingleton<ApplicationSettings>();

            /*
            var serviceProvider = services.BuildServiceProvider();
            JobSchedule.Start(serviceProvider);*/

            //services.AddSingleton<IHostedService, JobSchedule>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Login}/{id?}");
                routes.MapRoute("usuario", "{controller=Usuario}/{action=Index}/{id?}");
                routes.MapRoute("parametro", "{controller=Parametro}/{action=Index}/{id?}");
                routes.MapRoute("organizacion", "{controller=Organizacion}/{action=Index}/{id?}");
                routes.MapRoute("rol", "{controller=Rol}/{action=Index}/{id?}");
                routes.MapRoute("contrato", "{controller=Contrato}/{action=Index}/{id?}");
                routes.MapRoute("auditoria", "{controller=Auditoria}/{action=Index}/{id?}");
                routes.MapRoute("dashboard", "{controller=Dashboard}/{action=Index}/{id?}");
            });

            loggerFactory.AddLog4Net();
            
        }
    }
}
