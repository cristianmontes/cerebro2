﻿using System;
using System.Collections.Generic;

namespace cerebro.Models
{
    public partial class TbUnidad
    {
        public TbUnidad()
        {
            //InverseIdUnidadPadreNavigation = new HashSet<TbUnidad>();
        }

        public int IdUnidad { get; set; }
        public int? IdUnidadPadre { get; set; }
        public string Nombre { get; set; }
        public int IdTipoUnidad { get; set; }
        public string Estado { get; set; }
        public int Nivel { get; set; }
        public string Correo { get; set; }
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }

        /*
        public virtual TbUnidad IdUnidadPadreNavigation { get; set; }
        public virtual ICollection<TbUnidad> InverseIdUnidadPadreNavigation { get; set; }*/
    }
}
