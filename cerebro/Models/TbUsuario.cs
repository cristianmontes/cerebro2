﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace cerebro.Models
{
    public partial class TbUsuario
    {
        public TbUsuario()
        {
            
        }

        public int IdUsuario { get; set; }
        public int IdRol { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Usuario { get; set; }
        public string Correo { get; set; }
        public string Estado { get; set; }
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }

        public virtual TbRol IdRolNavigation { get; set; }

        [NotMapped]
        public string Password { get; set; }
    }
}
