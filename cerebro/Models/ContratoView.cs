﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Models
{
    public class ContratoView
    {
        public int[] SelectedAnios { get; set; }
        public int[] SelectedMeses { get; set; }
        public int[] SelectedSeveridades { get; set; }
        public int[] SelectedSituaciones { get; set; }
        public int[] SelectedCategorias { get; set; }
        public int[] SelectedRubros { get; set; }
        public int[] SelectedProveedores { get; set; }
        public int[] SelectedSubgerencias { get; set; }
        public int[] SelectedEstadosRev { get; set; }
        public int[] SelectedVigencias { get; set; }


        public IEnumerable<Models.TbContrato> Contratos { get; set; }
        public Models.TbContrato Contrato { get; set; }
    }
}
