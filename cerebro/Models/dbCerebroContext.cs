﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace cerebro.Models
{
    public partial class dbCerebroContext : DbContext
    {
        public dbCerebroContext()
        {
        }

        public dbCerebroContext(DbContextOptions<dbCerebroContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbAuditoria> TbAuditoria { get; set; }
        public virtual DbSet<TbContrato> TbContrato { get; set; }
        public virtual DbSet<TbContratoImporte> TbContratoImporte { get; set; }
        public virtual DbSet<TbDeTablas> TbDeTablas { get; set; }
        public virtual DbSet<TbRol> TbRol { get; set; }
        public virtual DbSet<TbUsuario> TbUsuario { get; set; }

        public virtual DbSet<TbUnidad> TbUnidad { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS01;Database=dbCerebro;Trusted_Connection=True;");
            }*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<TbAuditoria>(entity =>
            {
                entity.HasKey(e => e.IdAuditoria)
                    .HasName("tb_auditoria_pk");

                entity.ToTable("tb_auditoria");

                entity.Property(e => e.IdAuditoria).HasColumnName("id_auditoria");
                entity.Property(e => e.IdContrato).HasColumnName("id_contrato");
                entity.Property(e => e.NuContrato).HasColumnName("nu_contrato");
                entity.Property(e => e.DeContrato).HasColumnName("de_contrato");
                entity.Property(e => e.UsuarioOperacion).HasColumnName("usuario_operacion");
                entity.Property(e => e.FeOperacion)
                    .HasColumnName("fe_operacion")
                    .HasColumnType("date");

                entity.Property(e => e.TipoOperacion).HasColumnName("tipo_operacion");
                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

               entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TbContrato>(entity =>
            {
                entity.HasKey(e => e.IdContrato)
                    .HasName("tb_contrato_pk");

                entity.ToTable("tb_contrato");

                entity.Property(e => e.IdContrato).HasColumnName("id_contrato");

                entity.Property(e => e.AnioIniRelacion)
                    .HasColumnName("anio_ini_relacion")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.AniosContrato)
                    .HasColumnName("anios_contrato")
                    .HasColumnType("numeric(2, 1)");

                entity.Property(e => e.BusinessCase)
                    .HasColumnName("business_case");

                entity.Property(e => e.CantMeses).HasColumnName("cant_meses");

                entity.Property(e => e.Cantidad).HasColumnName("cantidad");

                entity.Property(e => e.CantidadMeses).HasColumnName("cantidad_meses");

                entity.Property(e => e.Capexeado)
                    .HasColumnName("capexeado");

                entity.Property(e => e.ClausulaSalida)
                    .HasColumnName("clausula_salida")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CoProyActual)
                    .HasColumnName("co_proy_actual")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CoProyAnterior)
                    .HasColumnName("co_proy_anterior")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeAreaFuncional)
                    //.IsRequired()
                    .HasColumnName("de_area_funcional")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeCategoria)
                    .HasColumnName("de_categoria")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeClasificacion)
                    .HasColumnName("de_clasificacion")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeFrecPago)
                    .HasColumnName("de_frec_pago")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeMoneda).HasColumnName("de_moneda");

                entity.Property(e => e.DeRubro).HasColumnName("de_rubro");

                entity.Property(e => e.DeSeveridad)
                    .HasColumnName("de_severidad")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeSituacion)
                    .HasColumnName("de_situacion")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeSubgerencia)
                    //.IsRequired()
                    .HasColumnName("de_subgerencia")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeTipoConsumo)
                    .HasColumnName("de_tipo_consumo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeTipoGasto).HasColumnName("de_tipo_gasto");

                entity.Property(e => e.DeTipoObligacion)
                    .HasColumnName("de_tipo_obligacion")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DeUnidadMedida)
                    .HasColumnName("de_unidad_medida")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DeVigencia)
                    //.IsRequired()
                    .HasColumnName("de_vigencia")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("descripcion")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DetallePenalidad)
                    .HasColumnName("detalle_penalidad")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.DiasPorVencer).HasColumnName("dias_por_vencer");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeFinContrato)
                    .HasColumnName("fe_fin_contrato")
                    .HasColumnType("date");

                entity.Property(e => e.FeIniContrato)
                    .HasColumnName("fe_ini_contrato")
                    .HasColumnType("date");

                entity.Property(e => e.FeIniRelacion)
                    .HasColumnName("fe_ini_relacion")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

                entity.Property(e => e.IdAreaFuncional).HasColumnName("id_area_funcional");

                entity.Property(e => e.IdCategoria).HasColumnName("id_categoria");

                entity.Property(e => e.IdClasificacion).HasColumnName("id_clasificacion");

                entity.Property(e => e.IdContratoPadre).HasColumnName("id_contrato_padre");

                entity.Property(e => e.IdFrecPago).HasColumnName("id_frec_pago");

                entity.Property(e => e.IdMoneda).HasColumnName("id_moneda");

                entity.Property(e => e.IdParaRevision).HasColumnName("id_para_revision");

                entity.Property(e => e.IdRubro).HasColumnName("id_rubro");

                entity.Property(e => e.IdSeveridad).HasColumnName("id_severidad");

                entity.Property(e => e.IdSituacion).HasColumnName("id_situacion");

                entity.Property(e => e.IdSubgerencia).HasColumnName("id_subgerencia");

                entity.Property(e => e.IdTipoConsumo).HasColumnName("id_tipo_consumo");

                entity.Property(e => e.IdTipoGasto).HasColumnName("id_tipo_gasto");

                entity.Property(e => e.IdTipoObligacion).HasColumnName("id_tipo_obligacion");

                entity.Property(e => e.IdUnidadMedida).HasColumnName("id_unidad_medida");

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdVigencia).HasColumnName("id_vigencia");

                entity.Property(e => e.ImpoUnitConIgv)
                    .HasColumnName("impo_unit_con_igv")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.ImpoUnitSinIgv)
                    .HasColumnName("impo_unit_sin_igv")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.NuContrato)
                    .HasColumnName("nu_contrato")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.NuProveedor).HasColumnName("nu_proveedor");

                entity.Property(e => e.Observacion)
                    //.IsRequired()
                    .HasColumnName("observacion")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Penalidad)
                    .HasColumnName("penalidad");

                entity.Property(e => e.Proveedor)
                    .HasColumnName("proveedor")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Proyecto)
                    //.IsRequired()
                    .HasColumnName("proyecto")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReglAnioRevi).HasColumnName("regl_anio_revi");

                entity.Property(e => e.ReglMesRevi).HasColumnName("regl_mes_revi");

                entity.Property(e => e.ReglaFechaRevision)
                    .HasColumnName("regla_fecha_revision")
                    .HasColumnType("date");

                entity.Property(e => e.ReglaMesesPrevios).HasColumnName("regla_meses_previos");

                entity.Property(e => e.RenoAuto)
                    .HasColumnName("reno_auto");

                entity.Property(e => e.ResponsableTi)
                    .HasColumnName("responsable_ti")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReviOpexAnioSiguiente).HasColumnName("revi_opex_anio_siguiente");

                entity.Property(e => e.Impuestos)
                    .HasColumnName("impuestos")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.TipoCambio)
                    .HasColumnName("tipo_cambio")
                    .HasColumnType("numeric(12, 2)");

                entity.Property(e => e.ImporteTotalMN)
                    .HasColumnName("importe_totalmn")
                    .HasColumnType("numeric(20, 2)");

                /*
                entity.HasOne(d => d.IdContratoPadreNavigation)
                    .WithMany(p => p.InverseIdContratoPadreNavigation)
                    .HasForeignKey(d => d.IdContratoPadre)
                    .HasConstraintName("tb_contrato_tb_contrato");*/

                entity.Property(e => e.IdEstadoRev).HasColumnName("id_estado_rev");
                entity.Property(e => e.DeEstadoRev)
                    .HasColumnName("de_estado_rev")
                    .HasMaxLength(50)
                    .IsUnicode(false);
                entity.Property(e => e.UrlContrato)
                    .HasColumnName("urlcontrato")
                    .HasMaxLength(350)
                    .IsUnicode(false);

                entity.Property(e => e.NotaReno)
                    .HasColumnName("nota_reno")
                    .HasMaxLength(350)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<TbContratoImporte>(entity =>
            {
                entity.HasKey(e => e.IdContratoImporte)
                    .HasName("tb_contrato_importe_pk");

                entity.ToTable("tb_contrato_importe");

                entity.Property(e => e.IdContratoImporte).HasColumnName("id_contrato_importe");

                entity.Property(e => e.Anio).HasColumnName("anio");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.Clasificacion)
                    .HasColumnName("clasificacion")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

                entity.Property(e => e.IdContrato).HasColumnName("id_contrato");

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ImpoAnualIgv)
                    .HasColumnName("impo_anual_igv")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.ImpoTotalIgv)
                    .HasColumnName("impo_total_igv")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.Observacion)
                    //.IsRequired()
                    .HasColumnName("observacion")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IdEstrategia).HasColumnName("id_estrategia");

                entity.Property(e => e.NroMeses).HasColumnName("nro_meses");

                entity.Property(e => e.Igv)
                    .HasColumnName("igv")
                    .HasColumnType("numeric(20, 2)");

                entity.Property(e => e.ImpoTotalSoles)
                    .HasColumnName("impo_total_soles")
                    .HasColumnType("numeric(20, 2)");

                /*
                entity.HasOne(d => d.IdContratoNavigation)
                    .WithMany(p => p.TbContratoImporte)
                    .HasForeignKey(d => d.IdContrato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tb_contrato_importe_tb_contrato");*/
            });

            modelBuilder.Entity<TbDeTablas>(entity =>
            {
                entity.HasKey(e => e.IdTabla)
                    .HasName("tb_de_tablas_pk");

                entity.ToTable("tb_de_tablas");

                entity.Property(e => e.IdTabla).HasColumnName("id_tabla");

                entity.Property(e => e.Codigo)
                    .HasColumnName("codigo")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)                    
                    .HasColumnName("descripcion")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

                entity.Property(e => e.IdTablaPadre).HasColumnName("id_tabla_padre");

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    //.IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .HasColumnName("valor")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTablaPadreNavigation)
                    .WithMany(p => p.InverseIdTablaPadreNavigation)
                    .HasForeignKey(d => d.IdTablaPadre)
                    .HasConstraintName("fk_tb_tablas_de_tablas");
            });

            modelBuilder.Entity<TbRol>(entity =>
            {
                entity.HasKey(e => e.IdRol)
                    .HasName("tb_rol_pk");

                entity.ToTable("tb_rol");

                entity.Property(e => e.IdRol).HasColumnName("id_rol");

                entity.Property(e => e.Descripcion)
                    //.IsRequired()
                    .HasColumnName("descripcion")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TbUsuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario)
                    .HasName("tb_usuario_pk");

                entity.ToTable("tb_usuario");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Apellidos)
                    //.IsRequired()
                    .HasColumnName("apellidos")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Correo)
                    .HasColumnName("correo")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");

                entity.Property(e => e.IdRol).HasColumnName("id_rol");

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nombres)
                    //.IsRequired()
                    .HasColumnName("nombres")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Usuario)
                    .HasColumnName("usuario")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.TbUsuario)
                    .HasForeignKey(d => d.IdRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_tb_usuario_tb_rol_01");
            });

            modelBuilder.Entity<TbUnidad>(entity =>
            {
                entity.HasKey(e => e.IdUnidad)
                    .HasName("tb_unidad_pk");

                entity.ToTable("tb_unidad");

                entity.Property(e => e.IdUnidad).HasColumnName("id_unidad");
                entity.Property(e => e.IdUnidadPadre).HasColumnName("id_unidad_padre");
                entity.Property(e => e.Nombre)
                    //.IsRequired()
                    .HasColumnName("nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);
                entity.Property(e => e.IdTipoUnidad).HasColumnName("id_tipo_unidad");
                entity.Property(e => e.Estado).HasColumnName("estado");
                entity.Property(e => e.Nivel).HasColumnName("nivel");

                entity.Property(e => e.Correo)
                    .HasColumnName("correo")
                    .HasMaxLength(250)

                    .IsUnicode(false);
                entity.Property(e => e.FeCrea)
                    .HasColumnName("fe_crea")
                    .HasColumnType("date");

                entity.Property(e => e.FeModi)
                    .HasColumnName("fe_modi")
                    .HasColumnType("date");                

                entity.Property(e => e.IdUsuaCrea)
                    //.IsRequired()
                    .HasColumnName("id_usua_crea")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.IdUsuaModi)
                    .HasColumnName("id_usua_modi")
                    .HasMaxLength(20)
                    .IsUnicode(false);


                
                /*
                entity.HasOne(d => d.IdUnidadPadreNavigation)
                    .WithMany(p => p.InverseIdUnidadPadreNavigation)
                    .HasForeignKey(d => d.IdUnidadPadre)
                    .HasConstraintName("fk_tb_unidad_unidad");*/
            });

            modelBuilder.HasSequence("Sequence_1");
        }
    }
}
