﻿using System;
using System.Collections.Generic;

namespace cerebro.Models
{
    public partial class TbContratoImporte
    {
        public int IdContratoImporte { get; set; }
        public int IdContrato { get; set; }
        public int Anio { get; set; }
        public decimal ImpoAnualIgv { get; set; }
        public decimal ImpoTotalIgv { get; set; }
        public string Observacion { get; set; }
        public string Clasificacion { get; set; }
        public string Estado { get; set; }
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }
        public int IdEstrategia { get; set; }
        public int NroMeses { get; set; }
        public decimal Igv { get; set; }
        public decimal ImpoTotalSoles { get; set; }

        //public virtual TbContrato IdContratoNavigation { get; set; }
    }
}
