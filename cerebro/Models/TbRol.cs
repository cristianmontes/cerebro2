﻿using System;
using System.Collections.Generic;

namespace cerebro.Models
{
    public partial class TbRol
    {
        public TbRol()
        {
            TbUsuario = new HashSet<TbUsuario>();
        }

        public int IdRol { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }

        public virtual ICollection<TbUsuario> TbUsuario { get; set; }
    }
}
