﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cerebro.Models
{
    public partial class TbContrato
    {
        public TbContrato()
        {
            //InverseIdContratoPadreNavigation = new HashSet<TbContrato>();
            //TbAuditoria = new HashSet<TbAuditoria>();
            TbContratoImporte = new List<TbContratoImporte>();
        }

        public int IdContrato { get; set; }
        public int? IdContratoPadre { get; set; }
        public string NuContrato { get; set; }
        public int? ReglAnioRevi { get; set; }
        public int? ReglMesRevi { get; set; }
        public int? IdCategoria { get; set; }
        public string DeCategoria { get; set; }
        public int? IdRubro { get; set; }
        public string DeRubro { get; set; }
        public string Descripcion { get; set; }
        public int NuProveedor { get; set; }
        public string Proveedor { get; set; }
        public int? IdSeveridad { get; set; }
        public string DeSeveridad { get; set; }
        public int? IdParaRevision { get; set; }
        public int? IdSituacion { get; set; }
        public string DeSituacion { get; set; }
        public decimal? AniosContrato { get; set; }
        public int? IdFrecPago { get; set; }
        public string DeFrecPago { get; set; }
        public int? CantMeses { get; set; }
        public int? IdTipoConsumo { get; set; }
        public string DeTipoConsumo { get; set; }
        public bool RenoAuto { get; set; }
        public int? IdMoneda { get; set; }
        public string DeMoneda { get; set; }
        public int? IdTipoGasto { get; set; }
        public string DeTipoGasto { get; set; }
        public string CoProyActual { get; set; }
        public string CoProyAnterior { get; set; }
        public int? IdClasificacion { get; set; }
        public string DeClasificacion { get; set; }
        public int? IdTipoObligacion { get; set; }
        public string DeTipoObligacion { get; set; }
        public int? ReviOpexAnioSiguiente { get; set; }
        public string ClausulaSalida { get; set; }
        public bool Penalidad { get; set; }
        public string DetallePenalidad { get; set; }
        [NotMapped]
        public string StrFeIniRelacion { get; set; }
        public DateTime? FeIniRelacion { get; set; }
        public string AnioIniRelacion { get; set; }
        [NotMapped]
        public string StrFeIniContrato { get; set; }
        public DateTime? FeIniContrato { get; set; }
        [NotMapped]
        public string StrFeFinContrato { get; set; }
        public DateTime? FeFinContrato { get; set; }
        public int? DiasPorVencer { get; set; }
        public int? CantidadMeses { get; set; }
        public int? ReglaMesesPrevios { get; set; }
        [NotMapped]
        public string StrReglaFechaRevision { get; set; }
        public DateTime? ReglaFechaRevision { get; set; }
        public bool Capexeado { get; set; }
        public string ResponsableTi { get; set; }
        public int? IdUnidadMedida { get; set; }
        public string DeUnidadMedida { get; set; }
        public decimal? ImpoUnitSinIgv { get; set; }
        public int? Cantidad { get; set; }
        public decimal? ImpoUnitConIgv { get; set; }
        public string Estado { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }
        public string IdUsuaModi { get; set; }
        public string IdUsuaCrea { get; set; }
        public int IdAreaFuncional { get; set; }
        public string DeAreaFuncional { get; set; }
        public bool BusinessCase { get; set; }
        public string Proyecto { get; set; }
        public string Observacion { get; set; }
        public int IdVigencia { get; set; }
        public string DeVigencia { get; set; }
        public int IdSubgerencia { get; set; }
        public string DeSubgerencia { get; set; }
        public decimal Impuestos { get; set; }
        public decimal TipoCambio { get; set; }
        public decimal ImporteTotalMN { get; set; }
        public int IdEstadoRev { get; set; }
        public string DeEstadoRev { get; set; }
        public string UrlContrato { get; set; }

        public string NotaReno { get; set; }
        /*
        public virtual TbContrato IdContratoPadreNavigation { get; set; }
        public virtual ICollection<TbContrato> InverseIdContratoPadreNavigation { get; set; }
        public virtual ICollection<TbAuditoria> TbAuditoria { get; set; }
        */
        [NotMapped]
        public List<TbContratoImporte> TbContratoImporte { get; set; }

    }
}
