﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Models
{
    public class AuditoriaView
    {
        public IEnumerable<Models.TbAuditoria> Auditorias { get; set; }
        public DateTime? FeDesde { get; set; }
        public DateTime? FeHasta { get; set; }

        public string StrFeDesde { get; set; }

        public string StrFeHasta { get; set; }
    }
}
