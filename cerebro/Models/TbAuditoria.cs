﻿using System;
using System.Collections.Generic;

namespace cerebro.Models
{
    public partial class TbAuditoria
    {
        public int IdAuditoria { get; set; }
        public int? IdContrato { get; set; }
        public string NuContrato { get; set; }
        public string DeContrato { get; set; }
        public string UsuarioOperacion { get; set; }
        public DateTime FeOperacion { get; set; }
        public string TipoOperacion { get; set; }
        public string Estado { get; set; }        
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }
    }
}
