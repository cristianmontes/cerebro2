﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Models
{
    public class ApplicationSettings
    {
        public string lda { get; set; }
        public string ServidorCorreo { get; set; }
        public string CorreoDefault { get; set; }
        public string Puerto { get; set; }
        public string ssl { get; set; }

        public ApplicationSettings(IConfiguration iConfiguration)
        {
            var smtpSection = iConfiguration.GetSection("ApplicationSettings");
            if (smtpSection != null)
            {
                lda = smtpSection.GetSection("lda").Value;
                ServidorCorreo = smtpSection.GetSection("ServidorCorreo").Value;
                CorreoDefault = smtpSection.GetSection("CorreoDefault").Value;
                Puerto = smtpSection.GetSection("Puerto").Value;
                ssl = smtpSection.GetSection("ssl").Value;
            }
        }

    }
}
