﻿using System;
using System.Collections.Generic;

namespace cerebro.Models
{
    public partial class TbDeTablas
    {
        public TbDeTablas()
        {
            InverseIdTablaPadreNavigation = new HashSet<TbDeTablas>();
        }

        public int IdTabla { get; set; }
        public int? IdTablaPadre { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Codigo { get; set; }
        public string Valor { get; set; }
        public string Estado { get; set; }
        public string IdUsuaCrea { get; set; }
        public string IdUsuaModi { get; set; }
        public DateTime FeCrea { get; set; }
        public DateTime? FeModi { get; set; }

        public virtual TbDeTablas IdTablaPadreNavigation { get; set; }
        public virtual ICollection<TbDeTablas> InverseIdTablaPadreNavigation { get; set; }
    }
}
