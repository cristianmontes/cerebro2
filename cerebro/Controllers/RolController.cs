﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using cerebro.Services;
using Microsoft.AspNetCore.Http;

namespace cerebro.Controllers
{
    public class RolController : Controller
    {
        private readonly dbCerebroContext _context;

        public RolController(dbCerebroContext context)
        {
            _context = context;
            
        }
        /*
        public RolController(IParametroService parametroService)
        {
            ParametroService = parametroService;
        }*/

        // GET: Rol
        public async Task<IActionResult> Index()
        {

            
            return View(await _context.TbRol.ToListAsync());
        }

        // GET: Rol/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbRol = await _context.TbRol
                .FirstOrDefaultAsync(m => m.IdRol == id);
            if (tbRol == null)
            {
                return NotFound();
            }

            return View(tbRol);
        }

        // GET: Rol/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rol/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Descripcion,Estado")] TbRol tbRol)
        {
            if (ModelState.IsValid)
            {
                tbRol.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                tbRol.FeCrea = DateTime.Now;
                _context.Add(tbRol);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbRol);
        }

        // GET: Rol/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbRol = await _context.TbRol.FindAsync(id);
            if (tbRol == null)
            {
                return NotFound();
            }
            return View(tbRol);
        }

        // POST: Rol/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdRol,Descripcion,Estado")] TbRol tbRol)
        {
            if (id != tbRol.IdRol)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TbRol tbRolbd = _context.TbRol.Find(id);
                    tbRolbd.Descripcion = tbRol.Descripcion;
                    tbRolbd.Estado = tbRol.Estado;
                    tbRolbd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                    tbRolbd.FeModi = DateTime.Now;

                    _context.Update(tbRolbd);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbRolExists(tbRol.IdRol))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbRol);
        }

        // GET: Rol/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbRol = await _context.TbRol
                .FirstOrDefaultAsync(m => m.IdRol == id);
            if (tbRol == null)
            {
                return NotFound();
            }

            return View(tbRol);
        }

        // POST: Rol/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tbRol = await _context.TbRol.FindAsync(id);
            _context.TbRol.Remove(tbRol);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbRolExists(int id)
        {
            return _context.TbRol.Any(e => e.IdRol == id);
        }
    }
}
