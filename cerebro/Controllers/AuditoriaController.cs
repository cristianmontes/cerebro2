﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using System.Globalization;

namespace cerebro.Controllers
{
    public class AuditoriaController : Controller
    {
        private readonly dbCerebroContext _context;
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(AuditoriaController));

        public AuditoriaController(dbCerebroContext context)
        {
            _context = context;
        }

        // GET: Auditoria
        public IActionResult Index()
        {
            AuditoriaView AuditoriaViewPage = new AuditoriaView();
            //AuditoriaViewPage.FeDesde = DateTime.Now;
            //AuditoriaViewPage.FeHasta = DateTime.Now;
            AuditoriaViewPage.Auditorias = Enumerable.Empty<TbAuditoria>();
            return View(AuditoriaViewPage);

            /*
            var dbCerebroContext = _context.TbAuditoria;
            return View(await dbCerebroContext.ToListAsync());*/
        }

        [HttpPost]
        public IActionResult Index(AuditoriaView auditoriaView)
        {
            var errors = ModelState
               .Where(x => x.Value.Errors.Count > 0)
               .Select(x => new { x.Key, x.Value.Errors })
               .ToArray();
            logger.Debug("errors " + errors.ToString());

            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                logger.Debug("message " + message);

            }

            auditoriaView.FeDesde = DateTime.ParseExact(auditoriaView.StrFeDesde, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            auditoriaView.FeHasta = DateTime.ParseExact(auditoriaView.StrFeHasta, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var auditorias = from m in _context.TbAuditoria
                             where m.FeOperacion.Date >= auditoriaView.FeDesde
                             where m.FeOperacion.Date <= auditoriaView.FeHasta
                             /*
                             where m.FeOperacion.CompareTo(auditoriaView.FeDesde) >=0 &&
                                m.FeOperacion.CompareTo(auditoriaView.FeHasta) <= 0*/
                             select m;
            /*
            auditorias.Where(x => DateTime.Compare(x.FeOperacion.Date, auditoriaView.FeDesde) >= 0 &&
                                    DateTime.Compare(x.FeOperacion.Date, auditoriaView.FeHasta) <= 0);*/
            logger.Debug("Desde: " + auditoriaView.FeDesde);
            logger.Debug("Hasta: " + auditoriaView.FeHasta);

            AuditoriaView AuditoriaViewPage = new AuditoriaView();
            AuditoriaViewPage.FeDesde = auditoriaView.FeDesde;
            AuditoriaViewPage.FeHasta = auditoriaView.FeHasta;
            AuditoriaViewPage.Auditorias = auditorias.ToList();
            return View(AuditoriaViewPage);

            /*
            var dbCerebroContext = _context.TbAuditoria;
            return View(await dbCerebroContext.ToListAsync());*/
        }
    }
}
