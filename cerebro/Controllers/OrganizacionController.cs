﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using Microsoft.AspNetCore.Http;
using cerebro.Services;

namespace cerebro.Controllers
{
    public class OrganizacionController : Controller
    {
        private readonly dbCerebroContext _context;

        public OrganizacionController(dbCerebroContext context)
        {
            _context = context;
        }

        // GET: Parametro
        public IActionResult Index()
        {
            //var applicationDbContext = _context.TbDeTablas.Include(t => t.IdTablaPadreNavigation);
            ViewData["IdTipoUnidad"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_TIPO_UNIDAD)).ToList(), "IdTabla", "Nombre");
            List<TbUnidad> unidades = _context.TbUnidad.OrderBy(s => s.IdUnidadPadre).ToList();
            List<TbUnidad> unidadesmod = new List<TbUnidad>();
            foreach (TbUnidad item in unidades)
            {
                if (item.IdUnidadPadre == null)
                {
                    unidadesmod.Add(item);
                }
                else
                {
                    int cont = 0;
                    foreach (TbUnidad itemmod in unidadesmod)
                    {
                        if (itemmod.IdUnidad == item.IdUnidadPadre) {
                            unidadesmod.Insert(cont+1, item);
                            break;
                        }
                        cont++;
                    }
                }
            }

            return View (unidadesmod);
        }

        

        public ActionResult GetUnidades()
        {
            List<TbUnidad> unidades = _context.TbUnidad.ToList();
            return Json(new { status = true, data = unidades });

        }

        public ActionResult Save(TbUnidad tbUnidad)
        {
            if (tbUnidad != null && tbUnidad.IdUnidadPadre != -1)
            {
                TbUnidad tbUnidadParent = _context.TbUnidad.Find(tbUnidad.IdUnidadPadre);
                tbUnidad.Nivel = tbUnidadParent.Nivel + 1;
            }
            else {
                tbUnidad.IdUnidadPadre = null;
                tbUnidad.Nivel = 0;
            }
            if (tbUnidad.IdUnidad == 0)
            {
                tbUnidad.FeCrea = DateTime.Now;
                tbUnidad.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                _context.Add(tbUnidad);
                _context.SaveChanges();
            }
            else
            {
                TbUnidad tbUnidadbd = _context.TbUnidad.Find(tbUnidad.IdUnidad);
                tbUnidadbd.IdUnidadPadre = tbUnidad.IdUnidadPadre;
                tbUnidadbd.Nombre = tbUnidad.Nombre;
                tbUnidadbd.IdTipoUnidad = tbUnidad.IdTipoUnidad;
                tbUnidadbd.Estado = tbUnidad.Estado;
                tbUnidadbd.Nivel = tbUnidad.Nivel;
                tbUnidadbd.Correo = tbUnidad.Correo;

                tbUnidadbd.FeModi = DateTime.Now;
                tbUnidadbd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                _context.Update(tbUnidadbd);
                _context.SaveChanges();
            }
            return Json(new { status = true, data = "" });
        }
    }
}
