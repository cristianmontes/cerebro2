﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using Microsoft.AspNetCore.Http;

namespace cerebro.Controllers
{
    public class UsuarioController : Controller
    {
        private readonly dbCerebroContext _context;

        public UsuarioController(dbCerebroContext context)
        {
            _context = context;
        }

        // GET: Usuarios
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.TbUsuario.Include(t => t.IdRolNavigation);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Usuarios/Details/5        
        [HttpGet]
        public JsonResult Details(int? id)
        {
            if (id == null)
            {
                return Json(NotFound());
            }

            var tbUsuario = _context.TbUsuario
                .Include(t => t.IdRolNavigation)
                .FirstOrDefault(m => m.IdUsuario == id);
            if (tbUsuario == null)
            {
                return Json(NotFound());
            }

            return Json(tbUsuario);
        }

        // GET: Usuarios/Create
        public IActionResult Create()
        {
            ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion");
            
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdRol,Nombres,Apellidos,Usuario,Correo,Estado")] TbUsuario tbUsuario)
        {
            if (ModelState.IsValid)
            {
                List<TbUsuario> listbyusuario = _context.TbUsuario.Where(s => s.Usuario == tbUsuario.Usuario).ToList();
                if (listbyusuario != null && listbyusuario.Count > 0)
                {
                    ViewData["message"] = "Ya existe un usuario registrado con el mismo código de usuario";
                    ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion");
                    return View(tbUsuario);
                }

                tbUsuario.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                tbUsuario.FeCrea = DateTime.Now;
                _context.Add(tbUsuario);
                _context.SaveChanges();
                
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion", tbUsuario.IdRol);
            return View(tbUsuario);
        }

        // GET: Usuarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbUsuario = await _context.TbUsuario.FindAsync(id);
            if (tbUsuario == null)
            {
                return NotFound();
            }
            ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion", tbUsuario.IdRol);
            return View(tbUsuario);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdUsuario,IdRol,Nombres,Apellidos,Usuario,Correo,Estado")] TbUsuario tbUsuario)
        {
            if (id != tbUsuario.IdUsuario)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    List<TbUsuario> listbyusuario = _context.TbUsuario.Where(s => s.Usuario == tbUsuario.Usuario).ToList();
                    if (listbyusuario != null && listbyusuario.Count > 0 && listbyusuario[0].IdUsuario != tbUsuario.IdUsuario)
                    {
                        ViewData["message"] = "Ya existe un usuario registrado con el mismo código de usuario";
                        ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion");
                        return View(tbUsuario);
                    }

                    TbUsuario tbUsuariobd = _context.TbUsuario.Find(id);
                    tbUsuariobd.IdRol = tbUsuario.IdRol;
                    tbUsuariobd.Nombres = tbUsuario.Nombres;
                    tbUsuariobd.Apellidos = tbUsuario.Apellidos;
                    tbUsuariobd.Usuario = tbUsuario.Usuario;
                    tbUsuariobd.Correo = tbUsuario.Correo;
                    tbUsuariobd.Estado = tbUsuario.Estado;

                    tbUsuariobd.FeModi = DateTime.Now;
                    tbUsuariobd.IdUsuaModi = HttpContext.Session.GetString("usuario");

                    _context.Update(tbUsuariobd);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbUsuarioExists(tbUsuario.IdUsuario))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdRol"] = new SelectList(_context.TbRol, "IdRol", "Descripcion", tbUsuario.IdRol);
            return View(tbUsuario);
        }

        // GET: Usuarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbUsuario = await _context.TbUsuario
                .Include(t => t.IdRolNavigation)
                .FirstOrDefaultAsync(m => m.IdUsuario == id);
            if (tbUsuario == null)
            {
                return NotFound();
            }

            return View(tbUsuario);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tbUsuario = await _context.TbUsuario.FindAsync(id);
            _context.TbUsuario.Remove(tbUsuario);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TbUsuarioExists(int id)
        {
            return _context.TbUsuario.Any(e => e.IdUsuario == id);
        }
    }
}
