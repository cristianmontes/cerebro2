﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using cerebro.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace cerebro.Controllers
{


    public class DashboardController : Controller
    {
        private readonly dbCerebroContext _context;
        public DashboardController(dbCerebroContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            string sql = "select t.color as color, count(1) as cantidad " +
                        "from(" +
                            "select case  " +
                            "when DATEDIFF(day, getdate(), regla_fecha_revision) <= 0 then 'Rojo' " +
                            "when DATEDIFF(day, getdate(), regla_fecha_revision) between 1 and 30 then 'Ambar' " +
                            "when DATEDIFF(day, getdate(), regla_fecha_revision) between 31 and 90 then 'Verde' " +
                            "when DATEDIFF(day, getdate(), regla_fecha_revision) >= 91 then 'Azul' " +
                        "end as color " +
                        "from dbo.tb_contrato where estado = 1 and regla_fecha_revision is not null) t " +
                        "group by t.color";

            CuadroView resultado = new CuadroView();
            resultado.resultado = new Dictionary<String, int>();
            var command = _context.Database.GetDbConnection().CreateCommand();
            command.CommandText = sql;

            _context.Database.OpenConnection();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    resultado.resultado[reader.GetString(0)] = reader.GetInt32(1);
                }
            }
            if (!resultado.resultado.ContainsKey("Ambar")) {
                resultado.resultado["Ambar"] = 0;
            }
            if (!resultado.resultado.ContainsKey("Azul"))
            {
                resultado.resultado["Azul"] = 0;
            }
            if (!resultado.resultado.ContainsKey("Verde"))
            {
                resultado.resultado["Verde"] = 0;
            }
            if (!resultado.resultado.ContainsKey("Rojo"))
            {
                resultado.resultado["Rojo"] = 0;
            }
            return View(resultado);
        }

        public ActionResult GetChild(string strlabel)
        {
            string sql = "select IsNull(c.de_subgerencia, 'Sin Asignar') as subgerencia, count(1) as cantidad " +
                            "from dbo.tb_contrato c " +
                             "where estado = 1 and ";
            if (strlabel.Equals("Rojo"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) <= 0 ";
            }
            else if (strlabel.Equals("Ambar"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) between 1 and 30 ";
            }
            else if (strlabel.Equals("Verde"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) between 31 and 90 ";
            }
            else
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) >= 91 ";
            }

            sql = sql + "group by c.de_subgerencia";


            Dictionary<String, int> resultado = new Dictionary<String, int>();
            var command = _context.Database.GetDbConnection().CreateCommand();
            command.CommandText = sql;

            _context.Database.OpenConnection();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    resultado[reader.GetString(0)] = reader.GetInt32(1);
                }
            }

            var resultadojson = from key in resultado.Keys
                                select new { subgerencia = key, cantidad = resultado[key] };
            return Json(new { status = true, data = resultadojson });

        }

        public ActionResult GetDetalleSubgerencia(string strlabel, string strsubgerencia)
        {
            string sql = "select descripcion, proveedor, fe_fin_contrato " +
                            "from dbo.tb_contrato c " +
                             "where estado = 1 and ";
            if ("Sin Asignar".Equals(strsubgerencia))
            {
                sql = sql + "de_subgerencia is null and ";
            }
            else {
                sql = sql + "de_subgerencia = '" + strsubgerencia + "' and ";
            }

            if (strlabel.Equals("Rojo"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) <= 0 ";
            }
            else if (strlabel.Equals("Ambar"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) between 1 and 30 ";
            }
            else if (strlabel.Equals("Verde"))
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) between 31 and 90 ";
            }
            else
            {
                sql = sql + "DATEDIFF(day, getdate(), regla_fecha_revision) >= 91 ";
            }

            var command = _context.Database.GetDbConnection().CreateCommand();
            command.CommandText = sql;

            _context.Database.OpenConnection();
            List<TbContrato> contratos = new List<TbContrato>();
            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    TbContrato TbContratoLocal = new TbContrato();
                    TbContratoLocal.Descripcion = reader.GetString(0);
                    TbContratoLocal.Proveedor = reader.GetString(1);
                    TbContratoLocal.FeFinContrato = reader.GetDateTime(2);

                    contratos.Add(TbContratoLocal);
                }
            }

            return Json(new { status = true, data = contratos });
        }
    }
}