﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using Microsoft.AspNetCore.Http;

namespace cerebro.Controllers
{
    public class ParametroController : Controller
    {
        private readonly dbCerebroContext _context;
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ParametroController));

        public ParametroController(dbCerebroContext context)
        {
            _context = context;
        }

        // GET: Parametro
        public async Task<IActionResult> Index()
        {
            //var applicationDbContext = _context.TbDeTablas.Include(t => t.IdTablaPadreNavigation);
            return View(await _context.TbDeTablas.Where(s => s.IdTablaPadre == null).ToListAsync());
        }

        // GET: Parametro/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbDeTablas = await _context.TbDeTablas
                .Include(t => t.IdTablaPadreNavigation)
                .FirstOrDefaultAsync(m => m.IdTabla == id);
            if (tbDeTablas == null)
            {
                return NotFound();
            }

            return View(tbDeTablas);
        }

        // GET: Parametro/Create
        public IActionResult Create()
        {
            ViewData["IdTablaPadre"] = new SelectList(_context.TbDeTablas, "IdTabla", "Nombre");
            return View();
        }

        // POST: Parametro/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("IdTablaPadre,Nombre,Descripcion,Codigo,Valor,Estado")] TbDeTablas tbDeTablas)
        {
            if (ModelState.IsValid)
            {
                if (tbDeTablas.IdTablaPadre == -1)
                {
                    tbDeTablas.IdTablaPadre = null;
                }
                List<TbDeTablas> listbyname = _context.TbDeTablas.Where(s => s.Nombre == tbDeTablas.Nombre).ToList();
                if (listbyname != null && listbyname.Count > 0)
                {
                    ViewData["message"] = "Ya existe un parámetro registrado con el mismo nombre";
                    return View(tbDeTablas);
                }
                if (tbDeTablas.Codigo != null && tbDeTablas.Codigo != "")
                {
                    List<TbDeTablas> listbycodigo = _context.TbDeTablas.Where(s => s.Codigo == tbDeTablas.Codigo).ToList();
                    if (listbycodigo != null && listbycodigo.Count > 0)
                    {
                        ViewData["message"] = "Ya existe un parámetro registrado con el mismo código";
                        return View(tbDeTablas);
                    }
                }

                

                tbDeTablas.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                tbDeTablas.FeCrea = DateTime.Now;
                _context.Add(tbDeTablas);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(tbDeTablas);
        }

        // GET: Parametro/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbDeTablas = await _context.TbDeTablas.FindAsync(id);
            if (tbDeTablas == null)
            {
                return NotFound();
            }

            ViewData["IdTablaPadre"] = new SelectList(_context.TbDeTablas, "IdTabla", "Nombre", tbDeTablas.IdTablaPadre);
            return View(tbDeTablas);
        }

        // POST: Parametro/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdTabla,IdTablaPadre,Nombre,Descripcion,Codigo,Valor,Estado")] TbDeTablas tbDeTablas)
        {
            if (id != tbDeTablas.IdTabla)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    TbDeTablas tbDeTablasbd = _context.TbDeTablas.Find(id);
                    tbDeTablasbd.Codigo = tbDeTablas.Codigo;
                    tbDeTablasbd.Descripcion = tbDeTablas.Descripcion;
                    tbDeTablasbd.Estado = tbDeTablas.Estado;
                    tbDeTablasbd.IdTabla = tbDeTablas.IdTabla;
                    tbDeTablasbd.IdTablaPadre = tbDeTablas.IdTablaPadre == -1 ? null : tbDeTablas.IdTablaPadre;
                    tbDeTablasbd.Nombre = tbDeTablas.Nombre;
                    tbDeTablasbd.Valor = tbDeTablas.Valor;

                    tbDeTablasbd.FeModi = DateTime.Now;
                    tbDeTablasbd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                    _context.Update(tbDeTablasbd);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TbDeTablasExists(tbDeTablas.IdTabla))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["IdTablaPadre"] = new SelectList(_context.TbDeTablas, "IdTabla", "Codigo", tbDeTablas.IdTablaPadre);
            return View(tbDeTablas);
        }

        public IActionResult Delete(TbDeTablas entidad)
        {
            logger.Debug("borrando el registro " + entidad.IdTabla);
            List<TbDeTablas> listChildren = _context.TbDeTablas.Where(s => s.IdTablaPadre == entidad.IdTabla).ToList();
            if (listChildren == null || listChildren.Count == 0)
            {
                var tbDeTablas = _context.TbDeTablas.Find(entidad.IdTabla);
                _context.TbDeTablas.Remove(tbDeTablas);
                _context.SaveChanges();
                return Json(new { status = true, data = "OK" });
            }
            else {
                return Json(new { status = true, data = "NOOK", message = "No puede eliminar un parametro que tiene detalle" });
            }            
        }

        public IActionResult DeleteChild(TbDeTablas entidad)
        {
            logger.Debug("borrando el registro " + entidad.IdTabla);

            string msjerror = "No puede eliminar este parametro por que se encuentra asociado a mas de 1 contrato";
            List<TbContrato> listContract = _context.TbContrato.Where(s => s.IdCategoria == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new { status = true, data = "NOOK", 
                    message = msjerror});
            }
            listContract = _context.TbContrato.Where(s => s.IdRubro == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new { status = true, data = "NOOK", 
                    message = msjerror});
            }
            listContract = _context.TbContrato.Where(s => s.IdSeveridad == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }

            listContract = _context.TbContrato.Where(s => s.IdParaRevision == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdSituacion == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdFrecPago == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdTipoConsumo == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdMoneda == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdTipoGasto == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdClasificacion == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdTipoObligacion == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }
            listContract = _context.TbContrato.Where(s => s.IdUnidadMedida == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }

            listContract = _context.TbContrato.Where(s => s.IdAreaFuncional == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }

            listContract = _context.TbContrato.Where(s => s.IdVigencia == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }

            listContract = _context.TbContrato.Where(s => s.IdEstadoRev == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }

            listContract = _context.TbContrato.Where(s => s.NuProveedor == entidad.IdTabla).ToList();
            if (listContract != null && listContract.Count > 0)
            {
                return Json(new
                {
                    status = true,
                    data = "NOOK",
                    message = msjerror
                });
            }


            var tbDeTablas = _context.TbDeTablas.Find(entidad.IdTabla);
            int? idtablapadre = tbDeTablas.IdTablaPadre;
            _context.TbDeTablas.Remove(tbDeTablas);
            _context.SaveChanges();
            List<TbDeTablas> listChildren = _context.TbDeTablas.Where(s => s.IdTablaPadre == idtablapadre).ToList();
            return Json(new { status = true, data = listChildren });
        }

        /*
        // GET: Parametro/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbDeTablas = await _context.TbDeTablas
                .Include(t => t.IdTablaPadreNavigation)
                .FirstOrDefaultAsync(m => m.IdTabla == id);
            if (tbDeTablas == null)
            {
                return NotFound();
            }

            return View(tbDeTablas);
        }

        // POST: Parametro/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tbDeTablas = await _context.TbDeTablas.FindAsync(id);
            _context.TbDeTablas.Remove(tbDeTablas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }*/

        private bool TbDeTablasExists(int id)
        {
            return _context.TbDeTablas.Any(e => e.IdTabla == id);
        }

        public ActionResult GetChildren(int idParent)
        {
            List<TbDeTablas> listChildren = _context.TbDeTablas.Where(s => s.IdTablaPadre == idParent).ToList();
            return Json(new { status = true, data = listChildren });

        }

        public ActionResult GetChild(int idTabla)
        {
            TbDeTablas tbDeTablas = _context.TbDeTablas.Find(idTabla);
            return Json(new { status = true, data = tbDeTablas });

        }

        public ActionResult SaveChild(TbDeTablas tbDeTablas)
        {
            if (tbDeTablas.IdTabla == 0)
            {

                List<TbDeTablas> listbyname = _context.TbDeTablas.Where(s => s.Nombre == tbDeTablas.Nombre).ToList();
                if (listbyname != null && listbyname.Count > 0)
                {
                    return Json(new { status = "NOOK", data = "", message = "Ya existe un parámetro registrado con el mismo nombre" });
                }
                if (tbDeTablas.Codigo != null && tbDeTablas.Codigo != "")
                {
                    List<TbDeTablas> listbycodigo = _context.TbDeTablas.Where(s => s.Codigo == tbDeTablas.Codigo).ToList();
                    if (listbycodigo != null && listbycodigo.Count > 0)
                    {
                        return Json(new { status = "NOOK", data = "NOOK", message = "Ya existe un parámetro registrado con el mismo código" });
                    }
                }

                tbDeTablas.FeCrea = DateTime.Now;
                tbDeTablas.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                _context.Add(tbDeTablas);
                _context.SaveChanges();
            }
            else
            {
                TbDeTablas tbDeTablasbd = _context.TbDeTablas.Find(tbDeTablas.IdTabla);
                tbDeTablasbd.Codigo = tbDeTablas.Codigo;
                tbDeTablasbd.Descripcion = tbDeTablas.Descripcion;
                tbDeTablasbd.Estado = tbDeTablas.Estado;
                tbDeTablasbd.Nombre = tbDeTablas.Nombre;
                tbDeTablasbd.Valor = tbDeTablas.Valor;

                tbDeTablasbd.FeModi = DateTime.Now;
                tbDeTablasbd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                _context.Update(tbDeTablasbd);
                _context.SaveChanges();
            }
            List<TbDeTablas> listChildren = _context.TbDeTablas.Where(s => s.IdTablaPadre == tbDeTablas.IdTablaPadre).ToList();
            return Json(new { status = "OK", data = listChildren });
        }
    }
}
