﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using cerebro.Models;
using cerebro.Services;
using Microsoft.AspNetCore.Http;


using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using System.Globalization;

namespace cerebro.Controllers
{
    public class ContratoController : Controller
    {
        private readonly dbCerebroContext _context;
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(ContratoController));

        private static ContratoView ContratoViewPageExp;

        private static ContratoView ContratoViewMantExp;

        public ContratoController(dbCerebroContext context)
        {
            _context = context;
        }

        // GET: Contrato
        public IActionResult Index()
        {

            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdVigencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_VIGENCIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");

            ContratoView ContratoViewPage = new ContratoView();
            ContratoViewPage.Contrato = new TbContrato();
            ContratoViewPage.Contratos = Enumerable.Empty<TbContrato>();

            ContratoController.ContratoViewMantExp = ContratoViewPage;


            return View(ContratoViewPage);
        }

        [HttpPost]
        public IActionResult Index(ContratoView contratoView)
        {
            return View(buscarMantenimiento(contratoView));
            /*
            var contratos = from m in _context.TbContrato
                            select m;

            contratos = contratos.Where(s => s.Estado == Constants.ESTADO_ACTIVO);

            if (contratoView.Contrato.NuProveedor != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.NuProveedor == contratoView.Contrato.NuProveedor);
            }
            if (contratoView.Contrato.IdCategoria != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.IdCategoria == contratoView.Contrato.IdCategoria);
            }
            if (contratoView.Contrato.IdRubro != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.IdRubro == contratoView.Contrato.IdRubro);
            }

            ContratoView ContratoViewPage = new ContratoView();
            ContratoViewPage.Contrato = contratoView.Contrato;
            ContratoViewPage.Contratos = contratos.ToList();

            ContratoController.ContratoViewMantExp = ContratoViewPage;


            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");

            return View(ContratoViewPage);*/
        }



        public IActionResult Recargar()
        {
            //return Index();
            //return View("Index", ContratoController.ContratoViewMantExp);
            return View("Index", buscarMantenimiento(ContratoController.ContratoViewMantExp));
            //return RedirectToAction(nameof(Index));
            //return RedirectToAction(nameof(Index), buscarMantenimiento(ContratoController.ContratoViewMantExp));

        }

        private ContratoView buscarMantenimiento(ContratoView contratoView) {
            var contratos = from m in _context.TbContrato
                            select m;

            contratos = contratos.Where(s => s.Estado == Constants.ESTADO_ACTIVO);

            if (contratoView.Contrato.NuProveedor != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.NuProveedor == contratoView.Contrato.NuProveedor);
            }
            if (contratoView.Contrato.IdCategoria != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.IdCategoria == contratoView.Contrato.IdCategoria);
            }
            if (contratoView.Contrato.IdRubro != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.IdRubro == contratoView.Contrato.IdRubro);
            }
            if (contratoView.Contrato.IdVigencia != Constants.SELECCION_NINGUNO)
            {
                contratos = contratos.Where(s => s.IdVigencia == contratoView.Contrato.IdVigencia);
            }

            ContratoView ContratoViewPage = new ContratoView();
            ContratoViewPage.Contrato = contratoView.Contrato;
            ContratoViewPage.Contratos = contratos.ToList();

            ContratoController.ContratoViewMantExp = ContratoViewPage;


            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdVigencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_VIGENCIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            
            return ContratoViewPage;
        }

            // GET: Contrato/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbContrato = await _context.TbContrato
                //.Include(t => t.IdContratoPadreNavigation)
                .FirstOrDefaultAsync(m => m.IdContrato == id);
            if (tbContrato == null)
            {
                return NotFound();
            }

            return View(tbContrato);
        }

        // GET: Contrato/Create
        public IActionResult Create()
        {

            cargarParametros();
            return View();
        }

        // POST: Contrato/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind(
            "Descripcion,NuProveedor,IdSeveridad,IdParaRevision,IdSituacion,IdFrecPago,IdTipoConsumo,IdTipoObligacion,ClausulaSalida," +
            "Penalidad,DetallePenalidad,Observacion,IdSubgerencia,ImpoUnitSinIgv,Impuestos,ImpoUnitConIgv,IdMoneda,TipoCambio,ImporteTotalMN," +
            "StrFeIniRelacion,AnioIniRelacion,StrFeIniContrato,StrFeFinContrato,DiasPorVencer,IdVigencia,AniosContrato,ReglaMesesPrevios,StrReglaFechaRevision," +
            "CantMeses,RenoAuto,Capexeado,IdCategoria,IdRubro,IdTipoGasto,IdClasificacion,IdAreaFuncional,BusinessCase,IdUnidadMedida,Cantidad," +
            "TbContratoImporte,UrlContrato,NotaReno")] TbContrato tbContrato)
        {
            var errors = ModelState
               .Where(x => x.Value.Errors.Count > 0)
               .Select(x => new { x.Key, x.Value.Errors })
               .ToArray();
            logger.Debug("errors " + errors.ToString());

            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                logger.Debug("message " + message);

            }
           

            if (ModelState.IsValid)
            /*logger.Debug("ModelState.IsValid" + ModelState.IsValid);
            if (errors == null || errors.Length == 0)*/
            {
                if (tbContrato.StrFeIniRelacion != null && tbContrato.StrFeIniRelacion.Length > 0)
                {
                    tbContrato.FeIniRelacion = DateTime.ParseExact(tbContrato.StrFeIniRelacion, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeIniContrato != null && tbContrato.StrFeIniContrato.Length > 0)
                {
                    tbContrato.FeIniContrato = DateTime.ParseExact(tbContrato.StrFeIniContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeFinContrato != null && tbContrato.StrFeFinContrato.Length > 0)
                {
                    tbContrato.FeFinContrato = DateTime.ParseExact(tbContrato.StrFeFinContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrReglaFechaRevision != null && tbContrato.StrReglaFechaRevision.Length > 0)
                {
                    tbContrato.ReglaFechaRevision = DateTime.ParseExact(tbContrato.StrReglaFechaRevision, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                logger.Debug("entro a grabar: " + tbContrato);
                if (tbContrato.NuProveedor != Constants.SELECCION_NINGUNO)
                    tbContrato.Proveedor = _context.TbDeTablas.Find(tbContrato.NuProveedor).Nombre;
                if (tbContrato.IdSeveridad != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSeveridad = _context.TbDeTablas.Find(tbContrato.IdSeveridad).Nombre;
                //if (tbContrato.IdParaRevision != Constants.SELECCION_NINGUNO)
                //tbContrato.DeParaRevision = _context.TbDeTablas.Find(tbContrato.IdParaRevision).Nombre;
                if (tbContrato.IdSituacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSituacion = _context.TbDeTablas.Find(tbContrato.IdSituacion).Nombre;
                if (tbContrato.IdFrecPago != Constants.SELECCION_NINGUNO)
                    tbContrato.DeFrecPago = _context.TbDeTablas.Find(tbContrato.IdFrecPago).Nombre;
                if (tbContrato.IdTipoConsumo != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoConsumo = _context.TbDeTablas.Find(tbContrato.IdTipoConsumo).Nombre;
                if (tbContrato.IdTipoObligacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoObligacion = _context.TbDeTablas.Find(tbContrato.IdTipoObligacion).Nombre;
                if (tbContrato.IdSubgerencia != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSubgerencia = _context.TbUnidad.Find(tbContrato.IdSubgerencia).Nombre;
                if (tbContrato.IdVigencia != Constants.SELECCION_NINGUNO)
                    tbContrato.DeVigencia = _context.TbDeTablas.Find(tbContrato.IdVigencia).Nombre;
                if (tbContrato.IdCategoria != Constants.SELECCION_NINGUNO)
                    tbContrato.DeCategoria = _context.TbDeTablas.Find(tbContrato.IdCategoria).Nombre;
                if (tbContrato.IdRubro != Constants.SELECCION_NINGUNO)
                    tbContrato.DeRubro = _context.TbDeTablas.Find(tbContrato.IdRubro).Nombre;
                if (tbContrato.IdTipoGasto != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoGasto = _context.TbDeTablas.Find(tbContrato.IdTipoGasto).Nombre;
                if (tbContrato.IdClasificacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeClasificacion = _context.TbDeTablas.Find(tbContrato.IdClasificacion).Nombre;
                if (tbContrato.IdAreaFuncional != Constants.SELECCION_NINGUNO)
                    tbContrato.DeAreaFuncional = _context.TbDeTablas.Find(tbContrato.IdAreaFuncional).Nombre;
                if (tbContrato.IdMoneda != Constants.SELECCION_NINGUNO)
                    tbContrato.DeMoneda = _context.TbDeTablas.Find(tbContrato.IdMoneda).Nombre;
                if (tbContrato.IdUnidadMedida != Constants.SELECCION_NINGUNO)
                    tbContrato.DeUnidadMedida = _context.TbDeTablas.Find(tbContrato.IdUnidadMedida).Nombre;

                tbContrato.IdEstadoRev = Constants.CODIGO_ESTREV_INICIADO;
                tbContrato.DeEstadoRev = _context.TbDeTablas.Find(tbContrato.IdEstadoRev).Nombre;
                tbContrato.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                tbContrato.IdUsuaModi = HttpContext.Session.GetString("usuario");
                tbContrato.FeCrea = DateTime.Now;
                tbContrato.Estado = Constants.ESTADO_ACTIVO;

                List<TbContratoImporte> importes = tbContrato.TbContratoImporte;
                _context.Add(tbContrato);
                _context.SaveChanges();


                foreach (var item in importes)
                {

                    item.IdContrato = tbContrato.IdContrato;
                    item.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                    item.FeCrea = DateTime.Now;
                    item.Estado = Constants.ESTADO_ACTIVO;

                    item.Clasificacion = item.Clasificacion == null ? "" : item.Clasificacion;
                    item.Observacion = item.Observacion == null ? "" : item.Observacion;

                    _context.Add(item);
                    _context.SaveChanges();
                }
                //return RedirectToAction(nameof(Index));
                return View("Index", buscarMantenimiento(ContratoController.ContratoViewMantExp));

            }
            ViewData["IdEstrategia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTRATEGIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            return View(tbContrato);
        }

        // GET: Contrato/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbContrato = await _context.TbContrato.FindAsync(id);
            if (tbContrato == null)
            {
                return NotFound();
            }
            tbContrato.StrFeIniRelacion = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniRelacion != null ? tbContrato.FeIniRelacion : DateTime.Now);
            tbContrato.StrFeIniContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniContrato != null ? tbContrato.FeIniContrato : DateTime.Now);
            tbContrato.StrFeFinContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeFinContrato != null ? tbContrato.FeFinContrato : DateTime.Now);
            tbContrato.StrReglaFechaRevision = string.Format("{0:dd/MM/yyyy}", tbContrato.ReglaFechaRevision != null ? tbContrato.ReglaFechaRevision : DateTime.Now);
            List<TbContratoImporte> importes = _context.TbContratoImporte.Where(s => s.IdContrato == id).ToList();
            tbContrato.TbContratoImporte = importes;

            cargarParametros();


            return View(tbContrato);
        }

        // POST: Contrato/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("IdContrato,IdContratoPadre,NuContrato," +
            "IdCategoria,IdRubro,Descripcion,NuProveedor,IdSeveridad," +
            "IdParaRevision,IdSituacion,AniosContrato,IdFrecPago,CantMeses,IdTipoConsumo," +
            "RenoAuto,IdMoneda,IdTipoGasto,CoProyActual,CoProyAnterior,IdClasificacion," +
            "IdTipoObligacion,ReviOpexAnioSiguiente,ClausulaSalida,Penalidad," +
            "DetallePenalidad,StrFeIniRelacion,AnioIniRelacion,StrFeIniContrato,StrFeFinContrato,DiasPorVencer,CantidadMeses," +
            "ReglaMesesPrevios,StrReglaFechaRevision,Capexeado,RenovaAuto,ResponsableTi,IdUnidadMedida," +
            "ImpoUnitSinIgv,Cantidad,ImpoUnitConIgv,Estado,FeCrea,FeModi,IdUsuaModi,IdUsuaCrea,IdAreaFuncional," +
            "Impuestos,TipoCambio,ImporteTotalMN," +
            "BusinessCase,Proyecto,Observacion,IdVigencia,IdSubgerencia, TbContratoImporte, UrlContrato, NotaReno")] TbContrato tbContrato)
        {

            var errors = ModelState
               .Where(x => x.Value.Errors.Count > 0)
               .Select(x => new { x.Key, x.Value.Errors })
               .ToArray();
            logger.Debug("errors " + errors.ToString());

            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                logger.Debug("message " + message);

            }

            if (id != tbContrato.IdContrato)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (tbContrato.StrFeIniRelacion != null && tbContrato.StrFeIniRelacion.Length > 0)
                {
                    tbContrato.FeIniRelacion = DateTime.ParseExact(tbContrato.StrFeIniRelacion, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeIniContrato != null && tbContrato.StrFeIniContrato.Length > 0)
                {
                    tbContrato.FeIniContrato = DateTime.ParseExact(tbContrato.StrFeIniContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeFinContrato != null && tbContrato.StrFeFinContrato.Length > 0)
                {
                    tbContrato.FeFinContrato = DateTime.ParseExact(tbContrato.StrFeFinContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrReglaFechaRevision != null && tbContrato.StrReglaFechaRevision.Length > 0)
                {
                    tbContrato.ReglaFechaRevision = DateTime.ParseExact(tbContrato.StrReglaFechaRevision, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                TbContrato tbContratobd = _context.TbContrato.Find(id);

                if (tbContrato.IdCategoria != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeCategoria = _context.TbDeTablas.Find(tbContrato.IdCategoria).Nombre;
                if (tbContrato.IdRubro != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeRubro = _context.TbDeTablas.Find(tbContrato.IdRubro).Nombre;
                if (tbContrato.IdSeveridad != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeSeveridad = _context.TbDeTablas.Find(tbContrato.IdSeveridad).Nombre;
                //if (tbContrato.IdParaRevision != Constants.SELECCION_NINGUNO)
                //tbContrato.DeParaRevision = _context.TbDeTablas.Find(tbContrato.IdParaRevision).Nombre;
                if (tbContrato.IdSituacion != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeSituacion = _context.TbDeTablas.Find(tbContrato.IdSituacion).Nombre;
                if (tbContrato.IdFrecPago != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeFrecPago = _context.TbDeTablas.Find(tbContrato.IdFrecPago).Nombre;
                if (tbContrato.IdTipoConsumo != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeTipoConsumo = _context.TbDeTablas.Find(tbContrato.IdTipoConsumo).Nombre;
                if (tbContrato.IdMoneda != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeMoneda = _context.TbDeTablas.Find(tbContrato.IdMoneda).Nombre;
                if (tbContrato.IdTipoGasto != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeTipoGasto = _context.TbDeTablas.Find(tbContrato.IdTipoGasto).Nombre;
                if (tbContrato.IdClasificacion != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeClasificacion = _context.TbDeTablas.Find(tbContrato.IdClasificacion).Nombre;
                if (tbContrato.IdTipoObligacion != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeTipoObligacion = _context.TbDeTablas.Find(tbContrato.IdTipoObligacion).Nombre;
                if (tbContrato.IdUnidadMedida != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeUnidadMedida = _context.TbDeTablas.Find(tbContrato.IdUnidadMedida).Nombre;
                if (tbContrato.IdAreaFuncional != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeAreaFuncional = _context.TbDeTablas.Find(tbContrato.IdAreaFuncional).Nombre;
                if (tbContrato.IdVigencia != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeVigencia = _context.TbDeTablas.Find(tbContrato.IdVigencia).Nombre;
                if (tbContrato.IdSubgerencia != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeSubgerencia = _context.TbUnidad.Find(tbContrato.IdSubgerencia).Nombre;
                if (tbContrato.NuProveedor != Constants.SELECCION_NINGUNO)
                    tbContrato.Proveedor = _context.TbDeTablas.Find(tbContrato.NuProveedor).Nombre;

                tbContratobd.IdContratoPadre = tbContrato.IdContratoPadre;
                tbContratobd.NuContrato = tbContrato.NuContrato;
                tbContratobd.ReglAnioRevi = tbContrato.ReglAnioRevi;
                tbContratobd.ReglMesRevi = tbContrato.ReglMesRevi;
                tbContratobd.IdCategoria = tbContrato.IdCategoria;
                tbContratobd.IdRubro = tbContrato.IdRubro;
                tbContratobd.Descripcion = tbContrato.Descripcion;
                tbContratobd.NuProveedor = tbContrato.NuProveedor;
                tbContratobd.Proveedor = tbContrato.Proveedor;
                tbContratobd.IdSeveridad = tbContrato.IdSeveridad;
                tbContratobd.IdParaRevision = tbContrato.IdParaRevision;
                tbContratobd.IdSituacion = tbContrato.IdSituacion;
                tbContratobd.AniosContrato = tbContrato.AniosContrato;
                tbContratobd.IdFrecPago = tbContrato.IdFrecPago;
                tbContratobd.CantMeses = tbContrato.CantMeses;
                tbContratobd.IdTipoConsumo = tbContrato.IdTipoConsumo;
                tbContratobd.RenoAuto = tbContrato.RenoAuto;
                tbContratobd.IdMoneda = tbContrato.IdMoneda;
                tbContratobd.IdTipoGasto = tbContrato.IdTipoGasto;
                tbContratobd.CoProyActual = tbContrato.CoProyActual;
                tbContratobd.CoProyAnterior = tbContrato.CoProyAnterior;
                tbContratobd.IdClasificacion = tbContrato.IdClasificacion;
                tbContratobd.IdTipoObligacion = tbContrato.IdTipoObligacion;
                tbContratobd.ReviOpexAnioSiguiente = tbContrato.ReviOpexAnioSiguiente;
                tbContratobd.ClausulaSalida = tbContrato.ClausulaSalida;
                tbContratobd.Penalidad = tbContrato.Penalidad;
                tbContratobd.DetallePenalidad = tbContrato.DetallePenalidad;
                tbContratobd.FeIniRelacion = tbContrato.FeIniRelacion;
                tbContratobd.AnioIniRelacion = tbContrato.AnioIniRelacion;
                tbContratobd.FeIniContrato = tbContrato.FeIniContrato;
                tbContratobd.FeFinContrato = tbContrato.FeFinContrato;
                tbContratobd.DiasPorVencer = tbContrato.DiasPorVencer;
                tbContratobd.CantidadMeses = tbContrato.CantidadMeses;
                tbContratobd.ReglaMesesPrevios = tbContrato.ReglaMesesPrevios;
                tbContratobd.ReglaFechaRevision = tbContrato.ReglaFechaRevision;
                tbContratobd.Capexeado = tbContrato.Capexeado;

                tbContratobd.ResponsableTi = tbContrato.ResponsableTi;
                tbContratobd.IdUnidadMedida = tbContrato.IdUnidadMedida;
                tbContratobd.ImpoUnitSinIgv = tbContrato.ImpoUnitSinIgv;
                tbContratobd.Cantidad = tbContrato.Cantidad;
                tbContratobd.ImpoUnitConIgv = tbContrato.ImpoUnitConIgv;
                //tbContratobd.Estado = tbContrato.Estado;
                tbContratobd.IdAreaFuncional = tbContrato.IdAreaFuncional;
                tbContratobd.BusinessCase = tbContrato.BusinessCase;
                tbContratobd.Proyecto = tbContrato.Proyecto;
                tbContratobd.Observacion = tbContrato.Observacion;
                tbContratobd.IdVigencia = tbContrato.IdVigencia;
                tbContratobd.IdSubgerencia = tbContrato.IdSubgerencia;

                tbContratobd.UrlContrato = tbContrato.UrlContrato;
                tbContratobd.NotaReno = tbContrato.NotaReno;

                tbContratobd.Impuestos = tbContrato.Impuestos;
                tbContratobd.TipoCambio = tbContrato.TipoCambio;
                tbContratobd.ImporteTotalMN = tbContrato.ImporteTotalMN;

                tbContratobd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                tbContratobd.FeModi = DateTime.Now;

                _context.Update(tbContratobd);
                _context.SaveChanges();

                _context.TbContratoImporte.RemoveRange(
                    _context.TbContratoImporte.Where(x => x.IdContrato == tbContratobd.IdContrato));
                _context.SaveChanges();


                List<TbContratoImporte> importes = tbContrato.TbContratoImporte;

                foreach (var item in importes)
                {
                    item.IdContrato = tbContrato.IdContrato;

                    item.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                    item.FeCrea = DateTime.Now;
                    item.IdUsuaModi = HttpContext.Session.GetString("usuario");
                    item.FeModi = DateTime.Now;
                    item.Estado = Constants.ESTADO_ACTIVO;

                    item.Clasificacion = item.Clasificacion == null ? "" : item.Clasificacion;
                    item.Observacion = item.Observacion == null ? "" : item.Observacion;

                    _context.Add(item);
                    _context.SaveChanges();
                }

                
                //return RedirectToAction(nameof(Index));
                return View("Index", buscarMantenimiento(ContratoController.ContratoViewMantExp));
            }

            ViewData["IdEstrategia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTRATEGIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            return View(tbContrato);
        }

        public async Task<IActionResult> Renovar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbContrato = await _context.TbContrato.FindAsync(id);
            if (tbContrato == null)
            {
                return NotFound();
            }
            List<TbContratoImporte> importes = _context.TbContratoImporte.Where(s => s.IdContrato == id).ToList();
            /*
            tbContrato.StrFeIniRelacion = tbContrato.FeIniRelacion?.ToString("dd/MM/yyyy");
            tbContrato.StrFeIniContrato = tbContrato.FeIniContrato?.ToString("dd/MM/yyyy");
            tbContrato.StrFeFinContrato = tbContrato.FeFinContrato?.ToString("dd/MM/yyyy");
            tbContrato.StrReglaFechaRevision = tbContrato.ReglaFechaRevision?.ToString("dd/MM/yyyy");
            */

            tbContrato.StrFeIniRelacion = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniRelacion != null ? tbContrato.FeIniRelacion : DateTime.Now);
            tbContrato.StrFeIniContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniContrato != null ? tbContrato.FeIniContrato : DateTime.Now);
            tbContrato.StrFeFinContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeFinContrato != null ? tbContrato.FeFinContrato : DateTime.Now);
            tbContrato.StrReglaFechaRevision = string.Format("{0:dd/MM/yyyy}", tbContrato.ReglaFechaRevision != null ? tbContrato.ReglaFechaRevision : DateTime.Now);


            tbContrato.IdContratoPadre = tbContrato.IdContrato;
            tbContrato.IdContrato = 0;
            tbContrato.TbContratoImporte = importes;

            cargarParametros();


            return View(tbContrato);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Renovar([Bind(
            "IdContratoPadre, Descripcion,NuProveedor,IdSeveridad,IdParaRevision,IdSituacion,IdFrecPago,IdTipoConsumo,IdTipoObligacion,ClausulaSalida," +
            "Penalidad,DetallePenalidad,Observacion,IdSubgerencia,ImpoUnitSinIgv,Impuestos,ImpoUnitConIgv,IdMoneda,TipoCambio,ImporteTotalMN," +
            "StrFeIniRelacion,AnioIniRelacion,StrFeIniContrato,StrFeFinContrato,DiasPorVencer,IdVigencia,AniosContrato,ReglaMesesPrevios,StrReglaFechaRevision," +
            "CantMeses,RenoAuto,Capexeado,IdCategoria,IdRubro,IdTipoGasto,IdClasificacion,IdAreaFuncional,BusinessCase,IdUnidadMedida,Cantidad," +
            "TbContratoImporte, UrlContrato, NotaReno")] TbContrato tbContrato)
        {

            var errors = ModelState.Where(x => x.Value.Errors.Count > 0)
              .Select(x => new { x.Key, x.Value.Errors })
              .ToArray();

            logger.Debug("errors " + errors);

            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                logger.Debug("message " + message);

            }

            if (ModelState.IsValid)
            {

                if (tbContrato.StrFeIniRelacion != null && tbContrato.StrFeIniRelacion.Length > 0)
                {
                    tbContrato.FeIniRelacion = DateTime.ParseExact(tbContrato.StrFeIniRelacion, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeIniContrato != null && tbContrato.StrFeIniContrato.Length > 0)
                {
                    tbContrato.FeIniContrato = DateTime.ParseExact(tbContrato.StrFeIniContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrFeFinContrato != null && tbContrato.StrFeFinContrato.Length > 0)
                {
                    tbContrato.FeFinContrato = DateTime.ParseExact(tbContrato.StrFeFinContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrReglaFechaRevision != null && tbContrato.StrReglaFechaRevision.Length > 0)
                {
                    tbContrato.ReglaFechaRevision = DateTime.ParseExact(tbContrato.StrReglaFechaRevision, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (tbContrato.NuProveedor != Constants.SELECCION_NINGUNO)
                    tbContrato.Proveedor = _context.TbDeTablas.Find(tbContrato.NuProveedor).Nombre;
                if (tbContrato.IdSeveridad != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSeveridad = _context.TbDeTablas.Find(tbContrato.IdSeveridad).Nombre;
                //if (tbContrato.IdParaRevision != Constants.SELECCION_NINGUNO)
                //tbContrato.DeParaRevision = _context.TbDeTablas.Find(tbContrato.IdParaRevision).Nombre;
                if (tbContrato.IdSituacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSituacion = _context.TbDeTablas.Find(tbContrato.IdSituacion).Nombre;
                if (tbContrato.IdFrecPago != Constants.SELECCION_NINGUNO)
                    tbContrato.DeFrecPago = _context.TbDeTablas.Find(tbContrato.IdFrecPago).Nombre;
                if (tbContrato.IdTipoConsumo != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoConsumo = _context.TbDeTablas.Find(tbContrato.IdTipoConsumo).Nombre;
                if (tbContrato.IdTipoObligacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoObligacion = _context.TbDeTablas.Find(tbContrato.IdTipoObligacion).Nombre;
                if (tbContrato.IdSubgerencia != Constants.SELECCION_NINGUNO)
                    tbContrato.DeSubgerencia = _context.TbUnidad.Find(tbContrato.IdSubgerencia).Nombre;
                if (tbContrato.IdVigencia != Constants.SELECCION_NINGUNO)
                    tbContrato.DeVigencia = _context.TbDeTablas.Find(tbContrato.IdVigencia).Nombre;
                if (tbContrato.IdCategoria != Constants.SELECCION_NINGUNO)
                    tbContrato.DeCategoria = _context.TbDeTablas.Find(tbContrato.IdCategoria).Nombre;
                if (tbContrato.IdRubro != Constants.SELECCION_NINGUNO)
                    tbContrato.DeRubro = _context.TbDeTablas.Find(tbContrato.IdRubro).Nombre;
                if (tbContrato.IdTipoGasto != Constants.SELECCION_NINGUNO)
                    tbContrato.DeTipoGasto = _context.TbDeTablas.Find(tbContrato.IdTipoGasto).Nombre;
                if (tbContrato.IdClasificacion != Constants.SELECCION_NINGUNO)
                    tbContrato.DeClasificacion = _context.TbDeTablas.Find(tbContrato.IdClasificacion).Nombre;
                if (tbContrato.IdAreaFuncional != Constants.SELECCION_NINGUNO)
                    tbContrato.DeAreaFuncional = _context.TbDeTablas.Find(tbContrato.IdAreaFuncional).Nombre;
                tbContrato.DeAreaFuncional = _context.TbDeTablas.Find(tbContrato.IdAreaFuncional).Nombre;
                if (tbContrato.IdMoneda != Constants.SELECCION_NINGUNO)
                    tbContrato.DeMoneda = _context.TbDeTablas.Find(tbContrato.IdMoneda).Nombre;
                if (tbContrato.IdUnidadMedida != Constants.SELECCION_NINGUNO)
                    tbContrato.DeUnidadMedida = _context.TbDeTablas.Find(tbContrato.IdUnidadMedida).Nombre;

                tbContrato.IdEstadoRev = Constants.CODIGO_ESTREV_INICIADO;
                tbContrato.DeEstadoRev = _context.TbDeTablas.Find(tbContrato.IdEstadoRev).Nombre;
                tbContrato.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                tbContrato.IdUsuaModi = HttpContext.Session.GetString("usuario");
                tbContrato.FeCrea = DateTime.Now;
                tbContrato.Estado = Constants.ESTADO_ACTIVO;

                List<TbContratoImporte> importes = tbContrato.TbContratoImporte;
                _context.Add(tbContrato);
                _context.SaveChanges();


                foreach (var item in importes)
                {

                    item.IdContrato = tbContrato.IdContrato;
                    item.IdUsuaCrea = HttpContext.Session.GetString("usuario");
                    item.FeCrea = DateTime.Now;
                    item.Estado = Constants.ESTADO_ACTIVO;

                    item.Clasificacion = item.Clasificacion == null ? "" : item.Clasificacion;
                    item.Observacion = item.Observacion == null ? "" : item.Observacion;

                    _context.Add(item);
                    _context.SaveChanges();
                }
                //return RedirectToAction(nameof(Index));
                return View("Index", buscarMantenimiento(ContratoController.ContratoViewMantExp));

            }
            ViewData["IdEstrategia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTRATEGIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            return View(tbContrato);
        }


        public IActionResult Delete(TbContrato Contrato)
        {
            logger.Debug("borrando el contrato " + Contrato.IdContrato);
            TbContrato tbContrato = _context.TbContrato.Find(Contrato.IdContrato);
            tbContrato.Estado = Constants.ESTADO_INACTIVO;
            tbContrato.IdUsuaModi = HttpContext.Session.GetString("usuario");
            tbContrato.FeModi = DateTime.Now;

            _context.Update(tbContrato);
            _context.SaveChanges();
            return Json(new { status = true, data = "OK" });
        }

        public void cargarParametros()
        {
            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSeveridad"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SEVERIDAD)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdParaRevision"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PARAMREVISION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSituacion"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SITUACION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdFrecPago"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_FRECPAGO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdTipoConsumo"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_TIPOCONSUMO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdMoneda"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_MONEDA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdTipoGasto"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_TIPOGASTO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdClasificacion"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CLASIFICACION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdTipoObligacion"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_TIPOOBLIGACION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdUnidadMedida"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_UNIDADMEDIDA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdAreaFuncional"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_AREAFUNCIONAL)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdVigencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_VIGENCIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            //ViewData["IdSubgerencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SUBGERENCIA)).ToList(), "IdTabla", "Nombre");
            ViewData["IdSubgerencia"] = new SelectList(_context.TbUnidad.Where(b => b.IdTipoUnidad.Equals(Constants.CODIGO_UNIDAD_SUBGERENCIA)).OrderBy(x => x.Nombre).ToList(), "IdUnidad", "Nombre");

            ViewData["IdEstrategia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTRATEGIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["NuProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
        }
        private bool TbContratoExists(int id)
        {
            return _context.TbContrato.Any(e => e.IdContrato == id);
        }


        public IActionResult ConsultaVenc()
        {
            ViewData["IdSeveridad"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SEVERIDAD)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSituacion"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SITUACION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSubgerencia"] = new SelectList(_context.TbUnidad.Where(b => b.IdTipoUnidad.Equals(Constants.CODIGO_UNIDAD_SUBGERENCIA)).OrderBy(x => x.Nombre).ToList(), "IdUnidad", "Nombre");
            ViewData["IdEstadoRevision"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTREVISION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdVigencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_VIGENCIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre"); 
            

            ContratoView ContratoViewPage = new ContratoView();
            ContratoViewPage.Contrato = new TbContrato();
            ContratoViewPage.Contratos = Enumerable.Empty<TbContrato>();
            ContratoController.ContratoViewPageExp = ContratoViewPage;
            return View(ContratoViewPage);
        }

        [HttpPost]
        public IActionResult ConsultaVenc(ContratoView contratoView)
        {
            return View(buscarConsultaVenc(contratoView));
        }

        public IActionResult RecargarConsultaVenc()
        {
            return View("ConsultaVenc", buscarConsultaVenc(ContratoController.ContratoViewPageExp));
        }

        private ContratoView buscarConsultaVenc(ContratoView contratoView) {
            var contratos = from m in _context.TbContrato
                            select m;

            if (contratoView.SelectedAnios != null && contratoView.SelectedAnios.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedAnios.Contains((int)s.ReglaFechaRevision.Value.Year));
            }
            if (contratoView.SelectedMeses != null && contratoView.SelectedMeses.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedMeses.Contains((int)s.ReglaFechaRevision.Value.Month));
            }
            if (contratoView.SelectedSeveridades != null && contratoView.SelectedSeveridades.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedSeveridades.Contains((int)s.IdSeveridad));
            }
            if (contratoView.SelectedSituaciones != null && contratoView.SelectedSituaciones.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedSituaciones.Contains((int)s.IdSituacion));
            }
            if (contratoView.SelectedCategorias != null && contratoView.SelectedCategorias.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedCategorias.Contains((int)s.IdCategoria));
            }
            if (contratoView.SelectedRubros != null && contratoView.SelectedRubros.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedRubros.Contains((int)s.IdRubro));
            }
            if (contratoView.SelectedProveedores != null && contratoView.SelectedProveedores.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedProveedores.Contains((int)s.NuProveedor));
            }
            if (contratoView.SelectedSubgerencias != null && contratoView.SelectedSubgerencias.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedSubgerencias.Contains((int)s.IdSubgerencia));
            }
            if (contratoView.SelectedEstadosRev != null && contratoView.SelectedEstadosRev.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedEstadosRev.Contains((int)s.IdEstadoRev));
            }
            if (contratoView.SelectedVigencias != null && contratoView.SelectedVigencias.Length > 0)
            {
                contratos = contratos.Where(s => contratoView.SelectedVigencias.Contains((int)s.IdVigencia));
            }

            ContratoView ContratoViewPage = new ContratoView();
            ContratoViewPage.SelectedAnios = contratoView.SelectedAnios;
            ContratoViewPage.SelectedMeses = contratoView.SelectedMeses;
            ContratoViewPage.SelectedSeveridades = contratoView.SelectedSeveridades;
            ContratoViewPage.SelectedSituaciones = contratoView.SelectedSituaciones;
            ContratoViewPage.SelectedCategorias = contratoView.SelectedCategorias;
            ContratoViewPage.SelectedRubros = contratoView.SelectedRubros;
            ContratoViewPage.SelectedProveedores = contratoView.SelectedProveedores;
            ContratoViewPage.SelectedSubgerencias = contratoView.SelectedSubgerencias;
            ContratoViewPage.SelectedEstadosRev = contratoView.SelectedEstadosRev;
            ContratoViewPage.SelectedVigencias = contratoView.SelectedVigencias;

            ContratoViewPage.Contrato = contratoView.Contrato;
            ContratoViewPage.Contratos = contratos.ToList();

            ContratoController.ContratoViewPageExp = ContratoViewPage;

            ViewData["IdSeveridad"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SEVERIDAD)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSituacion"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_SITUACION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdCategoria"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_CATEGORIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdRubro"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_RUBRO)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdProveedor"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_PROVEEDOR)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdSubgerencia"] = new SelectList(_context.TbUnidad.Where(b => b.IdTipoUnidad.Equals(Constants.CODIGO_UNIDAD_SUBGERENCIA)).OrderBy(x => x.Nombre).ToList(), "IdUnidad", "Nombre");
            ViewData["IdEstadoRevision"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTREVISION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");
            ViewData["IdVigencia"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_VIGENCIA)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre"); 
            
            return ContratoController.ContratoViewPageExp;
        }

        public async Task<IActionResult> Modificar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbContrato = await _context.TbContrato.FindAsync(id);
            if (tbContrato == null)
            {
                return NotFound();
            }
            tbContrato.StrFeIniRelacion = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniRelacion != null ? tbContrato.FeIniRelacion : DateTime.Now);
            tbContrato.StrFeIniContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeIniContrato != null ? tbContrato.FeIniContrato : DateTime.Now);
            tbContrato.StrFeFinContrato = string.Format("{0:dd/MM/yyyy}", tbContrato.FeFinContrato != null ? tbContrato.FeFinContrato : DateTime.Now);
            tbContrato.StrReglaFechaRevision = string.Format("{0:dd/MM/yyyy}", tbContrato.ReglaFechaRevision != null ? tbContrato.ReglaFechaRevision : DateTime.Now);

            List<TbContratoImporte> importes = _context.TbContratoImporte.Where(s => s.IdContrato == id).ToList();
            tbContrato.IdContratoPadre = tbContrato.IdContrato;
            tbContrato.IdContrato = 0;
            tbContrato.TbContratoImporte = importes;

            cargarParametros();
            ViewData["IdEstadoRev"] = new SelectList(_context.TbDeTablas.Where(b => b.IdTablaPadre.Equals(Constants.CODIGO_ESTREVISION)).OrderBy(x => x.Nombre).ToList(), "IdTabla", "Nombre");

            return View(tbContrato);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Modificar(int id, [Bind("IdContrato,StrFeFinContrato,IdSeveridad,DiasPorVencer,CantidadMeses," +
            "ReglaMesesPrevios,StrReglaFechaRevision,IdEstadoRev")] TbContrato tbContrato)
        {

            var errors = ModelState.Where(x => x.Value.Errors.Count > 0)
              .Select(x => new { x.Key, x.Value.Errors })
              .ToArray();

            logger.Debug("errors " + errors);

            if (!ModelState.IsValid)
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                logger.Debug("message " + message);

            }

            if (ModelState.IsValid)
            {
                
                if (tbContrato.StrFeFinContrato != null && tbContrato.StrFeFinContrato.Length > 0)
                {
                    tbContrato.FeFinContrato = DateTime.ParseExact(tbContrato.StrFeFinContrato, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (tbContrato.StrReglaFechaRevision != null && tbContrato.StrReglaFechaRevision.Length > 0)
                {
                    tbContrato.ReglaFechaRevision = DateTime.ParseExact(tbContrato.StrReglaFechaRevision, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                TbContrato tbContratobd = _context.TbContrato.Find(id);

                if (tbContrato.IdSeveridad != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeSeveridad = _context.TbDeTablas.Find(tbContrato.IdSeveridad).Nombre;
                if (tbContrato.IdEstadoRev != Constants.SELECCION_NINGUNO)
                    tbContratobd.DeEstadoRev = _context.TbDeTablas.Find(tbContrato.IdEstadoRev).Nombre;

                tbContratobd.IdSeveridad = tbContrato.IdSeveridad;
                tbContratobd.IdEstadoRev = tbContrato.IdEstadoRev;

                tbContratobd.FeFinContrato = tbContrato.FeFinContrato;
                tbContratobd.DiasPorVencer = tbContrato.DiasPorVencer;
                tbContratobd.CantidadMeses = tbContrato.CantidadMeses;
                tbContratobd.ReglaMesesPrevios = tbContrato.ReglaMesesPrevios;
                tbContratobd.ReglaFechaRevision = tbContrato.ReglaFechaRevision;

                tbContratobd.IdUsuaModi = HttpContext.Session.GetString("usuario");
                tbContratobd.FeModi = DateTime.Now;

                _context.Update(tbContratobd);
                _context.SaveChanges();
                //return RedirectToAction(nameof(ConsultaVenc));
                return View("ConsultaVenc", buscarConsultaVenc(ContratoController.ContratoViewPageExp));


            }

            return View(tbContrato);
        }

        public ActionResult GetTabla(int idTabla)
        {
            TbDeTablas tbDeTablas = _context.TbDeTablas.Find(idTabla);
            return Json(new { status = true, data = tbDeTablas });

        }

        public ActionResult LogCambios(int idContrato)
        {

            //WriteExcelFile();
            List<TbAuditoria> importes = _context.TbAuditoria.Where(s => s.IdContrato == idContrato).ToList();
            return Json(new { status = true, data = importes });
        }

        [HttpGet]
        public ActionResult ExportaVencido()
        {
            MemoryStream ms = new MemoryStream();
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Contratos" };

                sheets.Append(sheet);

                Row headerRow = new Row();

                Cell cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Categoría");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Rubro");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Descripción");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Subgerencia");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Proveedor");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fecha Inicio Relación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Inicio de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fin de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Cantidad de Meses Contratado");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Vigencia");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Estado Revisión");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fecha Regla Revisión");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Días por Vencer");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Regla Meses Previos");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Severidad para Prescindir");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Situación de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Frecuencia de pago");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Consumo");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Renovación Automática");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Moneda");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe sin IGV");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe con IGV");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Cambio");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe Total MN");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Gasto");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Clasificación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Obligación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Clausula de salida");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Penalidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Detalle de Penalidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Años de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Unidad de Medida");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Cantidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Proyecto");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Area Funcional");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("BusinessCase");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Estrategia");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Url Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Notas Renovación");
                headerRow.AppendChild(cell);

                sheetData.AppendChild(headerRow);

                if (ContratoController.ContratoViewPageExp != null &&
                ContratoController.ContratoViewPageExp.Contratos != null &&
                ContratoController.ContratoViewPageExp.Contratos.Count() > 0)
                {

                    foreach (TbContrato contrato in ContratoController.ContratoViewPageExp.Contratos)
                    {
                        Row newRow = new Row();
                        Cell cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeCategoria);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeRubro);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Descripcion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSubgerencia);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Proveedor);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeIniRelacion == null ? "" : ((DateTime)contrato.FeIniRelacion).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeIniContrato == null ? "" : ((DateTime)contrato.FeIniContrato).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeFinContrato == null ? "" : ((DateTime)contrato.FeFinContrato).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.CantMeses.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeVigencia);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeEstadoRev);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.ReglaFechaRevision == null ? "" : ((DateTime)contrato.ReglaFechaRevision).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();

                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.FeFinContrato == null ? "0" : ""+(contrato.FeFinContrato?.Date.Subtract(DateTime.Now.Date))?.Days);
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ReglaMesesPrevios.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSeveridad);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSituacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeFrecPago);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoConsumo);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.RenoAuto ? "Si" : "No");
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeMoneda);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImpoUnitSinIgv.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImpoUnitConIgv.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.TipoCambio.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImporteTotalMN.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoGasto);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeClasificacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoObligacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.ClausulaSalida);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Penalidad ? "Si" : "No");
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DetallePenalidad);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.AniosContrato.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeUnidadMedida);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.Cantidad.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Proyecto);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeAreaFuncional);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.BusinessCase ? "Si" : "No");
                        newRow.AppendChild(cellin);


                        var importes = from m in _context.TbDeTablas
                                       join tci in _context.TbContratoImporte on m.IdTabla equals tci.IdEstrategia
                                       where (tci.Anio == DateTime.Now.Year) && (contrato.IdContrato == tci.IdContrato)
                                       select m;

                        List<TbDeTablas> tblEstrategia = importes.ToList();

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(tblEstrategia == null || tblEstrategia.Count == 0? "": tblEstrategia.First().Nombre);
                        newRow.AppendChild(cellin);


                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.UrlContrato);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.NotaReno);
                        newRow.AppendChild(cellin);

                        sheetData.AppendChild(newRow);
                    }
                }

                workbookPart.Workbook.Save();

                document.Close();
                //ms.ToArray();

                var downloadFileName = "contratos.xlsx";
                //return File(fileSource, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", downloadFileName);
                //var fs = new FileStream("F:/TestNewData.xlsx", FileMode.Open);
                byte[] tmp = ms.ToArray();
                return File(tmp, "application/ocet-stream", downloadFileName);
            }
        }

        [HttpGet]
        public ActionResult ExportaMantenimiento()
        {
            MemoryStream ms = new MemoryStream();
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new SheetData();
                worksheetPart.Worksheet = new Worksheet(sheetData);

                Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Contratos" };

                sheets.Append(sheet);

                Row headerRow = new Row();

                Cell cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Categoría");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Rubro");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Descripción");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Subgerencia");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Proveedor");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fecha Inicio Relación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Inicio de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fin de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Cantidad de Meses Contratado");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Vigencia");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Estado Revisión");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Fecha Regla Revisión");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Días por Vencer");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Regla Meses Previos");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Severidad para Prescindir");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Situación de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Frecuencia de pago");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Consumo");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Renovación Automática");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Moneda");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe sin IGV");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe con IGV");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Cambio");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Importe Total MN");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Gasto");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Clasificación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Tipo de Obligación");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Clausula de salida");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Penalidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Detalle de Penalidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Años de Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Unidad de Medida");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Cantidad");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Proyecto");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Area Funcional");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("BusinessCase");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Estrategia");
                headerRow.AppendChild(cell);               

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Url Contrato");
                headerRow.AppendChild(cell);

                cell = new Cell();
                cell.DataType = CellValues.String;
                cell.CellValue = new CellValue("Notas Renovación");
                headerRow.AppendChild(cell);

                sheetData.AppendChild(headerRow);

                if (ContratoController.ContratoViewMantExp != null &&
                ContratoController.ContratoViewMantExp.Contratos != null &&
                ContratoController.ContratoViewMantExp.Contratos.Count() > 0)
                {

                    foreach (TbContrato contrato in ContratoController.ContratoViewMantExp.Contratos)
                    {
                        Row newRow = new Row();
                        Cell cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeCategoria);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeRubro);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Descripcion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSubgerencia);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Proveedor);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeIniRelacion == null ? "" : ((DateTime)contrato.FeIniRelacion).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeIniContrato == null ? "" : ((DateTime)contrato.FeIniContrato).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.FeFinContrato == null ? "" : ((DateTime)contrato.FeFinContrato).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.CantMeses.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeVigencia);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeEstadoRev);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.ReglaFechaRevision == null ? "" : ((DateTime)contrato.ReglaFechaRevision).ToString("dd/MM/yyyy"));
                        newRow.AppendChild(cellin);
                        cellin = new Cell();

                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.FeFinContrato == null ? "0" : "" + (contrato.FeFinContrato?.Date.Subtract(DateTime.Now.Date))?.Days);
                        newRow.AppendChild(cellin);
                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ReglaMesesPrevios.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSeveridad);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeSituacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeFrecPago);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoConsumo);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.RenoAuto ? "Si" : "No");
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeMoneda);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImpoUnitSinIgv.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImpoUnitConIgv.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.TipoCambio.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.ImporteTotalMN.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoGasto);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeClasificacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeTipoObligacion);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.ClausulaSalida);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Penalidad ? "Si" : "No");
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DetallePenalidad);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.AniosContrato.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeUnidadMedida);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.Number;
                        cellin.CellValue = new CellValue(contrato.Cantidad.ToString());
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.Proyecto);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.DeAreaFuncional);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.BusinessCase ? "Si" : "No");
                        newRow.AppendChild(cellin);

                        var importes = from m in _context.TbDeTablas
                                       join tci in _context.TbContratoImporte on m.IdTabla equals tci.IdEstrategia
                                       where (tci.Anio == DateTime.Now.Year) && (contrato.IdContrato == tci.IdContrato)
                                       select m;

                        List<TbDeTablas> tblEstrategia = importes.ToList();

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(tblEstrategia == null || tblEstrategia.Count == 0 ? "" : tblEstrategia.First().Nombre);
                        newRow.AppendChild(cellin);


                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.UrlContrato);
                        newRow.AppendChild(cellin);

                        cellin = new Cell();
                        cellin.DataType = CellValues.String;
                        cellin.CellValue = new CellValue(contrato.NotaReno);
                        newRow.AppendChild(cellin);

                        sheetData.AppendChild(newRow);
                    }
                }

                workbookPart.Workbook.Save();

                document.Close();
                //ms.ToArray();

                var downloadFileName = "contratosMant.xlsx";
                //return File(fileSource, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", downloadFileName);
                //var fs = new FileStream("F:/TestNewData.xlsx", FileMode.Open);
                byte[] tmp = ms.ToArray();
                return File(tmp, "application/ocet-stream", downloadFileName);
            }
        }
    }
}
