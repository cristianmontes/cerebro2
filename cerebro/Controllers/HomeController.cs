﻿
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using cerebro.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.DirectoryServices;
using System.Configuration;
using DocumentFormat.OpenXml.Drawing.Charts;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Options;
using Microsoft.Office.Interop.Excel;

namespace cerebro.Controllers
{
    public class HomeController : Controller
    {
        private readonly dbCerebroContext _context;
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(HomeController));
        public ApplicationSettings _applicationSettings { get; set; }
        public HomeController(dbCerebroContext context, ApplicationSettings applicationSettings)
        {
            _context = context;
            _applicationSettings = applicationSettings;
        }

        public IActionResult Index()
        {

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Login()
        {
            logger.Debug("cargando el login....");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Validate(TbUsuario tbUsuario)
        {
            var _user = _context.TbUsuario.Where(s => s.Usuario == tbUsuario.Usuario).FirstOrDefault();

            if (_user != null && _user.Estado == "0") {
                return Json(new { status = false, message = "El Usuario no tiene permisos para acceder al sistema" });
            }
            logger.Debug("logueando al usuario...." + _user);
            if (_user != null && tbUsuario.Usuario == _user.Usuario)
            {
                
                HttpContext.Session.SetString("usuario", tbUsuario.Usuario);
                HttpContext.Session.SetInt32("idrol", _user.IdRol);
                HttpContext.Session.SetString("Nombre", _user.Nombres + ' ' + _user.Apellidos);
                
                /*
                return Json(new { status = true, message = "Logueo Correcto", 
                    nombreUsuario = _user.Nombres + " " + _user.Apellidos,
                    idrol = _user.IdRol});
                */
                
                if (AutenticarUsuario(tbUsuario.Usuario, tbUsuario.Password))
                {
                    return Json(new { status = true, message = "Logueo Correcto", 
                    nombreUsuario = _user.Nombres + " " + _user.Apellidos,
                    idrol = _user.IdRol});
                }
                else
                {
                    return Json(new { status = false, message = "Usuario o clave inválido" });
                }
                

            }
            else
            {
                return Json(new { status = false, message = "Usuario no Registrado" });
            }

        }

        
        public static bool AutenticarUsuario(String iStrUsername, String iStrPassword)
        {

            String Path = "LDAP://DOMBIF.PERU";
            //var dd = _applicationSettings.lda;

            //string Path = _applicationSettings.lda;
            String _dominio = "DOMBIF";
            if (_dominio == null)
                _dominio = "DOMBIF";
            string domainAndUsername = _dominio + @"\" + iStrUsername;

            logger.Info("_path: " + Path);
            logger.Info("domainAndUsername: " + domainAndUsername);

            DirectoryEntry objADAM = new DirectoryEntry(Path, domainAndUsername, iStrPassword);
            DirectorySearcher objSearchADAM = new DirectorySearcher(objADAM);
            try
            {
                objSearchADAM.Filter = "(SAMAccountName=" + iStrUsername + ")";
                objSearchADAM.PropertiesToLoad.Add("cn");
                SearchResult srResultado = objSearchADAM.FindOne();
                if (null == srResultado)
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                return false;
            }
            return true;
        }
    }
}