﻿using cerebro.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Quartz;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Net.Security;

namespace cerebro.Services
{
    public class RevisaEstadoTask : IJob
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(RevisaEstadoTask));
        //private readonly dbCerebroContext _context;
        //private readonly IServiceProvider _provider;
        public RevisaEstadoTask() { }
        public dbCerebroContext _context { get; set; }
        public ApplicationSettings _applicationSettings { get; set; }
        public RevisaEstadoTask(dbCerebroContext context, ApplicationSettings applicationSettings)
        {
            _context = context;
            _applicationSettings = applicationSettings;
        }

        Task IJob.Execute(IJobExecutionContext context)
        {
            logger.Debug("iniciando la tarea de revision de contratos");
            
            List<TbContrato> contratosrevisar = _context.TbContrato.
                Where(s => s.Estado == Constants.ESTADO_ACTIVO && s.IdEstadoRev == Constants.CODIGO_ESTREV_INICIADO &&
                s.ReglaFechaRevision <= DateTime.Now && s.FeFinContrato > DateTime.Now).OrderBy(s => s.IdSubgerencia).ToList();
            logger.Debug("Contratos por revisar " + contratosrevisar.Count);
            TbContrato iteminicial = contratosrevisar != null && contratosrevisar.Count > 0 ?
                contratosrevisar[0] : null;

            List<TbContrato> listEnvio = new List<TbContrato>();
            foreach (TbContrato item in contratosrevisar)
            {   
                if (iteminicial.IdSubgerencia != item.IdSubgerencia)
                {
                    //1. Enviamos todo lo que teniamos hasta ahora
                    if (listEnvio.Count > 0)
                    {
                        procesarContratos(listEnvio, "Por Revisar", true);
                    }
                    //2. inicializamos todo de nuevo para el sgte lote
                    listEnvio = new List<TbContrato>();
                    listEnvio.Add(item);
                }
                else 
                {
                    listEnvio.Add(item);
                }
                //asignamos el item actual
                iteminicial = item;                
            }
            //para procesar lo ultimo
            if (listEnvio.Count > 0)
            {
                procesarContratos(listEnvio, "Por Revisar", true);
            }

            List<TbContrato> contratosvencidos = _context.TbContrato.
                Where(s => s.Estado == Constants.ESTADO_ACTIVO && 
                s.FeFinContrato <= DateTime.Now).OrderBy(s => s.IdSubgerencia).ToList();
            logger.Debug("Contratos vencidos " + contratosvencidos.Count);

            TbContrato iteminicialv = contratosvencidos != null && contratosvencidos.Count > 0 ?
                contratosvencidos[0] : null;

            List<TbContrato> listEnvioV = new List<TbContrato>();
            foreach (TbContrato item in contratosvencidos)
            {
                if (iteminicialv.IdSubgerencia != item.IdSubgerencia)
                {
                    //1. Enviamos todo lo que teniamos hasta ahora
                    if (listEnvioV.Count > 0)
                    {
                        procesarContratos(listEnvioV, "Vencidos", false);
                    }
                    //2. inicializamos todo de nuevo para el sgte lote
                    listEnvioV = new List<TbContrato>();
                    listEnvioV.Add(item);
                }
                else
                {
                    listEnvioV.Add(item);
                }
                //asignamos el item actual
                iteminicialv = item;
            }
            //para procesar lo ultimo
            if (listEnvioV.Count > 0)
            {
                procesarContratos(listEnvioV, "Vencidos", false);
            }

            return Task.CompletedTask;
        }

        public void procesarContratos(List<TbContrato> listaContratos, string estado, bool update)
        {
            TbContrato contratoini = listaContratos[0];
            string emailDestino = "";
            List<TbUnidad> unidades = _context.TbUnidad.Where(s => s.IdUnidad == contratoini.IdSubgerencia).ToList();
            foreach (TbUnidad unidad in unidades)
            {

                if (!String.IsNullOrEmpty(unidad.Correo))
                {
                    emailDestino = unidad.Correo;
                }
            }

            if (!String.IsNullOrEmpty(emailDestino))
            {
                string strbodyemail = getHeaderEmail();
                strbodyemail += "<h4>Estimados señores,</h4>" +
                                "<h4>Se notifican " + listaContratos.Count + " contratos " + estado + ":</h4>" +
                                "<br/>";
                strbodyemail += "<table>" +
                                "<tr>" +
                                    "<th>Categoría</th>" +
                                    "<th>Rubro</th>" +
                                    "<th>Descripción</th>" +
                                    "<th>Sub Gerencia</th>" +
                                    "<th>Proveedor</th>" +
                                    "<th>Inicio de Contrato</th>" +
                                    "<th>Fin de Contrato</th>" +
                                    "<th>Cantidad Meses contratado</th>" +
                                    "<th>Estado Revisión</th>" +
                                    "<th>Severidad</th>" +
                                    "<th>Situación</th>" +
                                    "<th>Moneda</th>" +
                                    "<th>Importe Con IGV</th>" +
                                    "<th>Notas de Renovación</th>" +
                                    "<th>Tipo de Obligación</th>" +
                                "</tr>";

                foreach (TbContrato contrato in listaContratos)
                {
                    strbodyemail +=
                        /*
                        "<tr>"+
                        "<td>" + ((contrato.DeCategoria is null)? "" : contrato.DeCategoria)+ "</td>" +
                        "<td>Housing</td>" +
                        "<td>Mantenimiento Servidores</td>" +
                        "<td>Producción e Infraestructura</td>" +
                        "<td>Bell Tech</td>" +
                        "<td>04/06/2020</td>" +
                        "<td ></td>" +
                        "<td>12</td>" +
                        "<td> Alertado</td>" +
                        "<td>Media</td>" +
                        "<td> En revisión</td>" +
                        "<td> Dólares</td>" +
                        "<td>130.000,00</td>" +
                        "<td>2019</td>" +
                        "<td> Recurrente</td>" +
                        "</tr>";
                        */
                        "<tr>" +
                        "<td>" + ((contrato.DeCategoria is null) ? "" : contrato.DeCategoria) + "</td>" +
                        "<td>" + ((contrato.DeRubro is null) ? "" : contrato.DeRubro) + "</td>" +
                        "<td>" + ((contrato.Descripcion is null) ? "" : contrato.Descripcion) + "</td>" +
                        "<td>" + ((contrato.DeSubgerencia is null) ? "" : contrato.DeSubgerencia) + "</td>" +
                        "<td>" + ((contrato.Proveedor is null) ? "" : contrato.Proveedor) + "</td>" +
                        "<td>" + ((contrato.FeIniContrato is null) ? "" : ((DateTime)contrato.FeIniContrato).ToString("dd/MM/yyyy")) + "</td>" +
                        "<td>" + ((contrato.FeFinContrato is null) ? "" : ((DateTime)contrato.FeFinContrato).ToString("dd/MM/yyyy")) + "</td>" +
                        "<td>" + ((contrato.CantMeses is null) ? "" : ""+contrato.CantMeses) + "</td>" +
                        "<td>" + ((contrato.DeEstadoRev is null) ? "" : contrato.DeEstadoRev) + "</td>" +
                        "<td>" + ((contrato.DeSeveridad is null) ? "" : contrato.DeSeveridad) + "</td>" +
                        "<td>" + ((contrato.DeSituacion is null) ? "" : contrato.DeSituacion) + "</td>" +
                        "<td>" + ((contrato.DeMoneda is null) ? "" : contrato.DeMoneda) + "</td>" +
                        "<td>" + ((contrato.ImpoUnitConIgv is null) ? "" : ""+contrato.ImpoUnitConIgv) + "</td>" +
                        "<td>" + ((contrato.NotaReno is null) ? "" : contrato.NotaReno) + "</td>" +
                        "<td>" + ((contrato.DeTipoObligacion is null) ? "" : contrato.DeTipoObligacion) + "</td>" +
                        "</tr>";
                }
                strbodyemail += "</table>";
                strbodyemail += getFooterEmail();
                logger.Debug("Por Vencer");
                logger.Debug("correo: " + emailDestino + " body email => " + strbodyemail);
                enviarCorreo(emailDestino, strbodyemail);

                if (update)
                {
                    foreach (TbContrato contrato in listaContratos)
                    {

                        contrato.IdEstadoRev = Constants.CODIGO_ESTREV_ALERTADO;
                        contrato.DeEstadoRev = _context.TbDeTablas.Find(contrato.IdEstadoRev).Nombre;
                        _context.Update(contrato);
                        _context.SaveChanges();
                    }
                }
            }
        }

        public void enviarCorreo(String correoDestino, string htmlContent)
        {
            /*var apiKey = "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";
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("cerebro.app@banbif.com", "Usuario Cerebro");
            var subject = "Notificaciones Cerebro";
            var to = new EmailAddress(correoDestino);
            List<EmailAddress> tos = new List<EmailAddress>();
            tos.Add(to);
            tos.Add(new EmailAddress("cmontes375@gmail.com"));

            //var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlContent);
            var msg = MailHelper.CreateSingleEmailToMultipleRecipients(from, tos, subject, "", htmlContent);
            var response = await client.SendEmailAsync(msg);
            logger.Debug(response.Headers);
            logger.Debug(response.StatusCode);
            */
            try
            {
                var FROM = _applicationSettings.CorreoDefault;
                var SERVIDOR = _applicationSettings.ServidorCorreo;
                var PUERTO = _applicationSettings.Puerto;
                var SSL = _applicationSettings.ssl;

                logger.Debug("FROM " + FROM );
                logger.Debug("SERVIDOR " + SERVIDOR);
                logger.Debug("PUERTO " + PUERTO );
                logger.Debug("SSL " + SSL);

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServer.Certificate);

                var correo = new MailMessage();
                correo.From = new MailAddress(FROM);
                correo.To.Add(correoDestino);
                correo.Subject = "Notificaciones Cerebro";
                correo.Body = htmlContent;
                correo.IsBodyHtml = true;
                correo.Priority = MailPriority.Normal;

                SmtpClient smtp = new SmtpClient();
                smtp.Credentials = new NetworkCredential();
                smtp.Credentials = new NetworkCredential(FROM, "");
                smtp.Host = SERVIDOR;
                smtp.Port = Convert.ToInt32(PUERTO);
                smtp.EnableSsl = Convert.ToBoolean(SSL);

                //Envia correo
                smtp.Send(correo);

                logger.Debug("Correo enviado");
            }
            catch (Exception e)
            {
                logger.Debug("Correo NO enviado");
                logger.Debug(e.ToString());
                logger.Error(e.Message);
            }
        }


        public static string getHeaderEmail() {
            return "<html xmlns = 'http://www.w3.org/1999/xhtml'>" +
                "<head runat = 'server'>" +
                "<meta http-equiv = 'Content-Type' content = 'text/html; charset=utf-8'/>" +
                "<title></title>" +
                "<style type = 'text/css'>" +
                "/* Cambios sobre la propia tabla */" +
                "table {" +
                "    border-collapse:collapse;" +
                "    border: 1px solid #CCC;" +
                "}" +
                "/* Espacio de relleno en celdas y cabeceras */" +
                "td, th {" +
                "    padding: 10px;" +
                "}" +
                "/* Modificación de estilos en cabeceras */" +
                "th {" +
                "    background:#337DFF;" +
                "    color:#FFF;" +
                "}" +
                "/* Modificación de estilos en celdas */" +
                "td {" +
                "    text-align:center;" +
                "    border: 1px solid #111;" +
                "    color:#333;" +
                "    font-size:10px;" +
                "}" +
                "p.MsoNormal {" +
                "    margin-bottom:.0001pt;" +
                "    font-size:11.0pt;" +
                "    font-family:'Calibri',sans-serif;" +
                "    margin-left: 0cm;" +
                "    margin-right: 0cm;" +
                "    margin-top: 0cm;" +
                "}" +
                "</style> " +
            "</head>" +
            "<body>" +
            "<form id='form1' runat='server'>" +
                "<div>" +
                    "<br/>";
        }

        public static string getFooterEmail() { 
            return "<br/>"+
                    "</div>" +
                    "</form>" +
                    "</body>" +
                    "</html>";
        }

    }
}
