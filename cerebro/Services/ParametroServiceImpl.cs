﻿using cerebro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Services
{
    
    public class ParametroServiceImpl: IParametroService
    {
        public dbCerebroContext DbContext;
        public Dictionary<string, string> Values = new Dictionary<string, string>();
        public ParametroServiceImpl(dbCerebroContext dbContext) {
            DbContext = dbContext;
        }
        public List<TbDeTablas> obtenerTodos() {
            return DbContext.TbDeTablas.ToList();
        }

        public void setData(string key, string value) {
            Values[key] = value;
        }

        public string getData(string key) {
            return Values[key];
        }
    }
}
