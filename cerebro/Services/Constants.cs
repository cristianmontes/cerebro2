﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Services
{
    public class Constants
    {
        public const int SELECCION_NINGUNO = -1;
        public const int CODIGO_RUBRO = 2;
        public const int CODIGO_CATEGORIA = 6;
        public const int CODIGO_PROVEEDOR = 60;

        public const int CODIGO_SEVERIDAD = 8;
        public const int CODIGO_PARAMREVISION = 9;
        public const int CODIGO_SITUACION = 10;
        public const int CODIGO_FRECPAGO = 11;
        public const int CODIGO_TIPOCONSUMO = 12;
        public const int CODIGO_MONEDA = 13;
        public const int CODIGO_TIPOGASTO = 14;
        public const int CODIGO_CLASIFICACION = 16;
        public const int CODIGO_TIPOOBLIGACION = 17;
        public const int CODIGO_UNIDADMEDIDA = 21;
        public const int CODIGO_AREAFUNCIONAL = 18;
        public const int CODIGO_VIGENCIA = 19;
        public const int CODIGO_SUBGERENCIA = 20;

        public const int CODIGO_ESTRATEGIA = 46;
        public const int CODIGO_ESTREVISION = 96;
        public const int CODIGO_ESTREV_INICIADO = 97;
        public const int CODIGO_ESTREV_ALERTADO = 98;

        public const int CODIGO_TIPO_UNIDAD = 101;
        public const int CODIGO_UNIDAD_SUBGERENCIA = 102;


        public const string ESTADO_ACTIVO = "1";
        public const string ESTADO_INACTIVO = "0";
    }
}
