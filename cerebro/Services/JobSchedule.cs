﻿using Microsoft.Extensions.Hosting;
using Quartz;
using Quartz.Impl;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace cerebro.Services
{
    public class JobSchedule
    {
        private static IScheduler scheduler;
        public static async void Start(IServiceProvider serviceProvider)
        {
            scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            scheduler.JobFactory = new JobFactoryCerebro(serviceProvider);
            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<RevisaEstadoTask>()
                .WithIdentity("jobContratos").Build();

            ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity("triggerContratos")

            .ForJob(job)
            //.WithCronSchedule("0 0 0/2 ? * *")
            .WithCronSchedule("0 * * ? * *")
            //.WithCronSchedule("* 0 0 ? * * *")
            .StartNow()
            .Build();

            scheduler.ScheduleJob(job, trigger);

        }
    }
}
