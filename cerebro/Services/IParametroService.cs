﻿using cerebro.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Services
{
    public interface IParametroService
    {
        List<TbDeTablas> obtenerTodos();
        void setData(string key, string value);
        string getData(string key);
    }
}
