﻿using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cerebro.Services
{
    public class JobFactoryCerebro : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public JobFactoryCerebro(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            //return (IJob)this._serviceProvider.GetService(bundle.JobDetail.JobType);
            return _serviceProvider.GetRequiredService<RevisaEstadoTask>();
        }

        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
}
